package com.tip.mefgps.database;

import android.util.Log;

public class LocationHandler{
	private final static String TAG = "Location";
	int id; 
	int base_id; 
	String date_entry; 
	double longitude; 
	double latitude; 
	String name; 
	int type;
	String searchTag;
	double amountables;

	/**======================= Default Constructor ========================**/

	public LocationHandler(){

	}

	public LocationHandler(int id, int base_id, String date_entry, double latitude,  double longitude, String name, int type,String searchTag, double amountables){
		this.id = id;
		this.base_id = base_id;
		this.date_entry = date_entry;
		this.longitude = longitude;
		this.latitude = latitude;
		this.name = name;
		this.type = type;
		this.amountables = amountables;
		this.searchTag = searchTag;
	}

	public LocationHandler( int base_id, String date_entry, double latitude,  double longitude, String name, int type, String searchTag, double amountables){
		this.base_id = base_id;
		this.date_entry = date_entry;
		this.longitude = longitude;
		this.latitude = latitude;
		this.name = name;
		this.type = type;
		this.amountables = amountables;
		this.searchTag = searchTag;
		Log.i(TAG, "Add location data : " + base_id + " date " + date_entry + " lat lon " + latitude + " " + longitude + " name " + name + " ty[e" + type);
	}

	public int getId(){
		return id;
	}
	public void setId(int id){
		this.id = id;
	}

	public int getBase_id(){
		return base_id;
	}
	public void setBase_id(int base_id){
		this.base_id = base_id;
	}

	public String getDate_entry(){
		return date_entry;
	}
	public void setDate_entry(String date_entry){
		this.date_entry = date_entry;
	}

	public double getLongitude(){
		return longitude;
	}
	public void setLongitude(double longitude){
		this.longitude = longitude;
	}

	public double getLatitude(){
		return latitude;
	}
	public void setLatitude(double latitude){
		this.latitude = latitude;
	}

	public String getName(){
		return name;
	}
	public void setName(String name){
		this.name = name;
	}

	public int getType(){
		return type;
	}
	public void setType(int type){
		this.type = type;
	}

	public String getSearchTag() {
		return searchTag;
	}

	public void setSearchTag(String searchTag) {
		this.searchTag = searchTag;
	}

	public double getAmountables() {
		return amountables;
	}

	public void setAmountables(double amountables) {
		this.amountables = amountables;
	}
}