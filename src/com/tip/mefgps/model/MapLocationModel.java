package com.tip.mefgps.model;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import com.tip.mefgps.R;
import com.tip.mefgps.base.FileUtils;
import com.tip.mefgps.constant.ConstantValue;
import com.tip.mefgps.constant.GlobalVariables;
import com.tip.mefgps.database.Database;
import com.tip.mefgps.database.LocationHandler;
import com.tip.mefgps.database.LocationInfoHandler;
import com.tip.mefgps.database.LocationTypeHandler;
import com.tip.mefgps.database.SubLocationHandler;

public class MapLocationModel {

	private static final String TAG = MapLocationModel.class.getSimpleName();
	private Activity mActivity;
	private Database db;

	public MapLocationModel(Activity mActivity) {
		this.mActivity = mActivity;
		db = new Database(mActivity);
		if(db.getLocationCount() <= 0)
			addLocation();
		if(db.getLocationTypeCount() <= 0)
			addLocationType();
		db.getDatabase(mActivity);
	}

	public void addLocation(){
		//		db.addLocation(new LocationHandler(1, GlobalVariables.instanceOf().getDate(), 14.63732, 121.100393, "SM MARIKINA", ConstantValue.MALL,"sale", 100));
		//		//mall detail
		//		db.addLocationInfo(new LocationInfoHandler(0, GlobalVariables.instanceOf().getDate(), "SM City Marikina is a shopping mall owned and managed by SM Prime Holdings, and is located in Calumpang, Marikina, Philippines. SM Supermalls is owned by Henry Sy, Sr.. SM City Marikina is the first shopping mall that has a wi-fi connection. SM City Marikina is a part of SM's 50th Anniversary and it was the first SM mall to open in 2008.It is the 31st SM Supermall built in the Philippines, 14th latest SM Mall built within Metro Manila, and 3rd among other SM malls opened in the eastern region of Metropolitan Manila, after SM Megamall in Mandaluyong City and SM Supercenter Pasig in Pasig City (excluding nearby SM City Taytay which is part of Rizal Province). and not Metro Manila .SM City Marikina is located in the Marikina River area which is currently being rehabilitated and developed, and the Marikina Riverbanks Center. It is situated along Marikina�Infanta Highway (Radial Road-6 or R6), Barangay Calumpang, Marikina City. This mall has a total of 60,000 square meter land area and a total of 124,877.85 square meter gross floor area. Currently, SM City Marikina is one of the largest malls in Metro Manila and the 2nd largest mall in the eastern region of Metro Manila in terms of land area and also its floor areas thereby aligning it with the GFA of nearby malls Sta. Lucia East Grandmall (largest), Robinsons Place Metro East and Riverbanks Centermall, all situated on Marcos Highway.SM City Marikina was opened to public on September 5, 2008, with opening ceremonies and ribbon cutting headed by then MMDA Chairman Bayani Fernando, and his wife, Marikina City Mayor Marides Fernando and other city officials, as well as the owner of SM Prime Holdings Henry Sy, and SM executives and in 2011 - 2012, the new facade changed the logo of SM City Marikina from old one to new one.", 
		//				"Background", 1, getBitmap(mActivity, R.drawable.ic_launcher)));
		//		db.addLocationInfo(new LocationInfoHandler(0, GlobalVariables.instanceOf().getDate(), "8am - 10pm", 
		//				"Schedule", 1, null));
		//		db.addLocationInfo(new LocationInfoHandler(0, GlobalVariables.instanceOf().getDate(), "0935123123\n32123123123\n123123123", 
		//				"Contacts", 1, null));
		//
		//
		//		db.addLocation(new LocationHandler(2, GlobalVariables.instanceOf().getDate(), 14.6526, 121.11507, "Marikina City Mall", ConstantValue.MALL,"sale", 100));
		//		db.addLocationInfo(new LocationInfoHandler(0, GlobalVariables.instanceOf().getDate(), "SM City Marikina is a shopping mall owned and managed by SM Prime Holdings, and is located in Calumpang, Marikina, Philippines. SM Supermalls is owned by Henry Sy, Sr.. SM City Marikina is the first shopping mall that has a wi-fi connection. SM City Marikina is a part of SM's 50th Anniversary and it was the first SM mall to open in 2008.It is the 31st SM Supermall built in the Philippines, 14th latest SM Mall built within Metro Manila, and 3rd among other SM malls opened in the eastern region of Metropolitan Manila, after SM Megamall in Mandaluyong City and SM Supercenter Pasig in Pasig City (excluding nearby SM City Taytay which is part of Rizal Province). and not Metro Manila .SM City Marikina is located in the Marikina River area which is currently being rehabilitated and developed, and the Marikina Riverbanks Center. It is situated along Marikina�Infanta Highway (Radial Road-6 or R6), Barangay Calumpang, Marikina City. This mall has a total of 60,000 square meter land area and a total of 124,877.85 square meter gross floor area. Currently, SM City Marikina is one of the largest malls in Metro Manila and the 2nd largest mall in the eastern region of Metro Manila in terms of land area and also its floor areas thereby aligning it with the GFA of nearby malls Sta. Lucia East Grandmall (largest), Robinsons Place Metro East and Riverbanks Centermall, all situated on Marcos Highway.SM City Marikina was opened to public on September 5, 2008, with opening ceremonies and ribbon cutting headed by then MMDA Chairman Bayani Fernando, and his wife, Marikina City Mayor Marides Fernando and other city officials, as well as the owner of SM Prime Holdings Henry Sy, and SM executives and in 2011 - 2012, the new facade changed the logo of SM City Marikina from old one to new one.", 
		//				"Background", 2, getBitmap(mActivity, R.drawable.ic_launcher)));
		//		db.addLocationInfo(new LocationInfoHandler(0, GlobalVariables.instanceOf().getDate(), "8am - 10pm", 
		//				"Schedule", 2, null));
		//		db.addLocationInfo(new LocationInfoHandler(0, GlobalVariables.instanceOf().getDate(), "0935123123\n32123123123\n123123123", 
		//				"Contacts", 2, null));
		//
		//		db.addSubLocation(new SubLocationHandler(0, GlobalVariables.instanceOf().getDate(), "Jollibee",   14.6526, 121.11507, 2));
		//		db.addSubLocation(new SubLocationHandler(0, GlobalVariables.instanceOf().getDate(), "McDonalds",   14.6526, 121.11507, 2));
		//		db.addSubLocation(new SubLocationHandler(0, GlobalVariables.instanceOf().getDate(), "PizzaHut",   14.6526, 121.11507, 2));
		//		db.addSubLocation(new SubLocationHandler(0, GlobalVariables.instanceOf().getDate(), "Shakeys",   14.6526, 121.11507, 2));
		//
		//
		//		db.addLocation(new LocationHandler(3, GlobalVariables.instanceOf().getDate(), 14.634331 , 121.082455, "Mall3", ConstantValue.MALL,"sale", 100));
		//		db.addLocation(new LocationHandler(4, GlobalVariables.instanceOf().getDate(), 14.660655, 121.112925, "Mall4", ConstantValue.MALL,"sale", 100));
		//		db.addLocation(new LocationHandler(5, GlobalVariables.instanceOf().getDate(), 14.638981,121.100222, "Mall5", ConstantValue.MALL,"sale", 100));
		//TF
		//		db.addLocation(new LocationHandler(1, GlobalVariables.instanceOf().getDate(), 14.666633,121.108276, "Goverment", ConstantValue.GOVERMENT));
		//		db.addLocation(new LocationHandler(2, GlobalVariables.instanceOf().getDate(), 14.667463,121.103813, "Goverment", ConstantValue.GOVERMENT));
		//		db.addLocation(new LocationHandler(3, GlobalVariables.instanceOf().getDate(), 14.645375,121.098835, "Goverment", ConstantValue.GOVERMENT));
		//		db.addLocation(new LocationHandler(4, GlobalVariables.instanceOf().getDate(), 14.650026,121.11068, "Goverment", ConstantValue.GOVERMENT));
		//		db.addLocation(new LocationHandler(5, GlobalVariables.instanceOf().getDate(), 14.637735,121.097805, "Goverment", ConstantValue.GOVERMENT));
		//
		//		db.addLocation(new LocationHandler(1, GlobalVariables.instanceOf().getDate(), 14.647734, 121.126098, "HOSPITAL", ConstantValue.HOSPITAL));
		//		db.addLocation(new LocationHandler(2, GlobalVariables.instanceOf().getDate(), 14.662282, 121.118987, "HOSPITAL", ConstantValue.HOSPITAL));
		//		db.addLocation(new LocationHandler(3, GlobalVariables.instanceOf().getDate(), 14.650906, 121.122506, "HOSPITAL", ConstantValue.HOSPITAL));
		//		db.addLocation(new LocationHandler(4, GlobalVariables.instanceOf().getDate(), 14.651736, 121.131990, "HOSPITAL", ConstantValue.HOSPITAL));
		//		db.addLocation(new LocationHandler(5, GlobalVariables.instanceOf().getDate(), 14.668219, 121.126926, "HOSPITAL", ConstantValue.HOSPITAL));
		//
		//		db.addLocation(new LocationHandler(1, GlobalVariables.instanceOf().getDate(),14.631474, 121.104310, "HOTEL", ConstantValue.HOTEL));
		//		db.addLocation(new LocationHandler(2, GlobalVariables.instanceOf().getDate(), 14.648830, 121.096671, "HOTEL", ConstantValue.HOTEL));
		//		db.addLocation(new LocationHandler(3, GlobalVariables.instanceOf().getDate(), 14.650823, 121.092808, "HOTEL", ConstantValue.HOTEL));
		//		db.addLocation(new LocationHandler(4, GlobalVariables.instanceOf().getDate(), 14.658546, 121.101477, "HOTEL", ConstantValue.HOTEL));
		//		db.addLocation(new LocationHandler(5, GlobalVariables.instanceOf().getDate(), 14.665271, 121.106627, "HOTEL", ConstantValue.HOTEL));

		db.addLocation(new LocationHandler(1, GlobalVariables.instanceOf().getDate(), 14.640265, 121.097167, "PMFTC Inc.", ConstantValue.ESTABLISHMENT,"service", 100));
		//		db.addLocationInfo(new LocationInfoHandler(0, 
		//				GlobalVariables.instanceOf().getDate(), 
		//				"", 
		//				"",
		//				1,
		//				getBitmap(mActivity, R.drawable.ic_launcher)));
		db.addLocationInfo(new LocationInfoHandler(0, 
				GlobalVariables.instanceOf().getDate(), 
				"PMFTC, Inc., formerly Fortune Tobacco Corporation is a private Philippine tobacco company that produces cigarettes, and with sister companies Tanduay Distillers and Asia Brewery, produces beer, liquor and bottled water. Originally the company was majority-owned by businessman Lucio Tan, who established it in 1966 and was controlled by the Philippine government, after it was seized by the Presidential Commission on Good Governance in 1987, when the Sandiganbayan returned the company to Tan.",
				"BACKGROUND",
				1,
				null));
		db.addLocationInfo(new LocationInfoHandler(0, 
				GlobalVariables.instanceOf().getDate(), 
				"Manufacturer", 
				"SERVICES",
				1,
				null));
		db.addLocationInfo(new LocationInfoHandler(0, 
				GlobalVariables.instanceOf().getDate(), 
				"Brewery, cigarettes", 
				"SPECIALIZATION",
				1,
				null));
		//		db.addLocationInfo(new LocationInfoHandler(0, 
		//				GlobalVariables.instanceOf().getDate(), 
		//				"Brewery, cigarettes", 
		//				"SPECIALIZATION",
		//				2,
		//				null));






		db.addLocation(new LocationHandler(2, GlobalVariables.instanceOf().getDate(), 14.651418, 121.111646, "Meralco-Mkna Br. Office", ConstantValue.ESTABLISHMENT,"service", 100));
		db.addLocationInfo(new LocationInfoHandler(0, 
				GlobalVariables.instanceOf().getDate(), 
				"It was organized as the Manila electric Railroad and Light Company 107 years ago in 1903 to provide electric light and power and an electric street railway system to Manila and its suburbs. The facilities that Meralco built to provide these two services represented for many years the largest single investment of American private capital and know-how in the whole of east-Asia. For a little than four decades, Meralco provided Manilans their first modern mass public transportation system with electric streetcars which in the twenties were supplemented by busses. World War 2 destroyed the railway system beyond rehabilitation and Meralco gave up its transportation business in 1948, concentrating thenceforth on providing electricity.  The electric service it provided powered much of the postwar rehabilitation and early industrialization of the young republic that became independent in 1946", 
				"BACKGROUND",
				2,
				null));
		db.addLocationInfo(new LocationInfoHandler(0, 
				GlobalVariables.instanceOf().getDate(), 
				"Franchise � Meralco", 
				"SERVICES ",
				2,
				null));
		db.addLocationInfo(new LocationInfoHandler(0, 
				GlobalVariables.instanceOf().getDate(), 
				"941 8817", 
				"CONTACT NO.",
				2,
				null));
		db.addLocation(new LocationHandler(3, GlobalVariables.instanceOf().getDate(), 14.671731, 121.129102, "ARMS Corporation of the Phil", ConstantValue.ESTABLISHMENT,"service", 100));

		db.addLocationInfo(new LocationInfoHandler(0, 
				GlobalVariables.instanceOf().getDate(), 
				"The Arms Corporation of the Philippines (Armscor) is a firearms manufacturing company headquartered in the Philippines. The company is known for its inexpensive 1911-pattern pistols, revolvers, shotguns, sporting rifles, firearms parts and ammunition", 
				"BACKGROUND",
				3,
				null));
		db.addLocationInfo(new LocationInfoHandler(0, 
				GlobalVariables.instanceOf().getDate(), 
				"Manufacturer � Arms and Ammo", 
				"SERVICES",
				3,
				null));
		db.addLocationInfo(new LocationInfoHandler(0, 
				GlobalVariables.instanceOf().getDate(), 
				"941 62 43", 
				"CONTACT NO.",
				4,
				null));
		db.addLocation(new LocationHandler(4, GlobalVariables.instanceOf().getDate(), 14.661204, 121.116386, "DELFI Marketing Inc.", ConstantValue.ESTABLISHMENT,"service", 100));
		db.addLocationInfo(new LocationInfoHandler(0, 
				GlobalVariables.instanceOf().getDate(), 
				"Other wholesaler", 
				"SERVICES",
				4,
				null));
		db.addLocationInfo(new LocationInfoHandler(0, 
				GlobalVariables.instanceOf().getDate(), 
				"Other wholesaler", 
				"CONTACT NO.",
				4,
				null));

		db.addLocation(new LocationHandler(5, GlobalVariables.instanceOf().getDate(), 14.626656, 121.084189, "SM Marikina Dept Store", ConstantValue.ESTABLISHMENT,"service", 100));
		db.addLocationInfo(new LocationInfoHandler(0, 
				GlobalVariables.instanceOf().getDate(), 
				"SM City Marikina is a shopping mall owned and managed by SM Prime Holdings, and is located in Calumpang, Marikina,Philippines. SM Supermalls is owned by Henry Sy, Sr.. SM City Marikina is the first shopping mall that has a wi-fi connection. SM City Marikina is a part of SM's 50th Anniversary and it was the first SM mall to open in 2008.", 
				"BACKGROUND",
				5,
				null));
		db.addLocationInfo(new LocationInfoHandler(0, 
				GlobalVariables.instanceOf().getDate(), 
				"Retailer � Department Store", 
				"SERVICES ",
				5,
				null));
		db.addLocationInfo(new LocationInfoHandler(0, 
				GlobalVariables.instanceOf().getDate(), 
				"857 01 00", 
				"CONTACT NO.",
				5,
				null));

		db.addLocation(new LocationHandler(6, GlobalVariables.instanceOf().getDate(), 14.640254, 121.097210, "Sanford Marketing Corp", ConstantValue.ESTABLISHMENT,"service", 100));
		//		db.addLocationInfo(new LocationInfoHandler(0, 
		//				GlobalVariables.instanceOf().getDate(), 
		//				"SM City Marikina is a shopping mall owned and managed by SM Prime Holdings, and is located in Calumpang, Marikina,Philippines. SM Supermalls is owned by Henry Sy, Sr.. SM City Marikina is the first shopping mall that has a wi-fi connection. SM City Marikina is a part of SM's 50th Anniversary and it was the first SM mall to open in 2008.", 
		//				"BACKGROUND",
		//				6,
		//				null));
		db.addLocationInfo(new LocationInfoHandler(0, 
				GlobalVariables.instanceOf().getDate(), 
				"Retailer � Supermarket", 
				"SERVICES ",
				6,
				null));
		//		db.addLocationInfo(new LocationInfoHandler(0, 
		//				GlobalVariables.instanceOf().getDate(), 
		//				"857 01 00", 
		//				"CONTACT NO.",
		//				6,
		//				null));


		db.addLocation(new LocationHandler(7, GlobalVariables.instanceOf().getDate(), 14.661173, 121.116322, "DELFI Food, Inc", ConstantValue.ESTABLISHMENT,"service", 100));
		//		db.addLocationInfo(new LocationInfoHandler(0, 
		//		GlobalVariables.instanceOf().getDate(), 
		//		"SM City Marikina is a shopping mall owned and managed by SM Prime Holdings, and is located in Calumpang, Marikina,Philippines. SM Supermalls is owned by Henry Sy, Sr.. SM City Marikina is the first shopping mall that has a wi-fi connection. SM City Marikina is a part of SM's 50th Anniversary and it was the first SM mall to open in 2008.", 
		//		"BACKGROUND",
		//		6,
		//		null));
		db.addLocationInfo(new LocationInfoHandler(0, 
				GlobalVariables.instanceOf().getDate(), 
				"Manufacturer � Chocolate/Candies", 
				"SERVICES ",
				7,
				null));
		db.addLocationInfo(new LocationInfoHandler(0, 
				GlobalVariables.instanceOf().getDate(), 
				"941 35 92", 
				"CONTACT NO.",
				7,
				null));

		db.addLocation(new LocationHandler(8, GlobalVariables.instanceOf().getDate(), 14.640285, 121.097124, "Tower Steel Corporation.", ConstantValue.ESTABLISHMENT,"service", 100));

		db.addLocationInfo(new LocationInfoHandler(0, 
				GlobalVariables.instanceOf().getDate(), 
				"Tower Steel Corporation is engaged in the manufacturing of G.I. sheets. We have been in the industry for more than 30 years. ", 
				"BACKGROUND",
				8,
				null));
		db.addLocationInfo(new LocationInfoHandler(0, 
				GlobalVariables.instanceOf().getDate(), 
				"Manufacturer � G.I Sheet/Steel", 
				"SERVICES ",
				8,
				null));
		db.addLocationInfo(new LocationInfoHandler(0, 
				GlobalVariables.instanceOf().getDate(), 
				"635 98 88", 
				"CONTACT NO.",
				8,
				null));

		db.addLocation(new LocationHandler(9, GlobalVariables.instanceOf().getDate(), 14.640358, 121.097157, "Manila Water Co. Inc.", ConstantValue.ESTABLISHMENT,"service", 100));
		db.addLocationInfo(new LocationInfoHandler(0, 
				GlobalVariables.instanceOf().getDate(), 
				"Manila Water Company Inc. is in the business of water collection, treatment and supply. As the private concessionaire of the Metropolitan Waterworks and Sewage Systems (MWSS), Manila water provides water and wastewater services to cities and municipalities of Eastern Metro Manila and Rizal Provinces. Now, the company operates in Cebu, Clark, Laguna, Boracay and Vietnam. The company formally took over operations for the East Zone in 1997, and its office is based in Quezon City.", 
				"BACKGROUND",
				9,
				null));
		db.addLocationInfo(new LocationInfoHandler(0, 
				GlobalVariables.instanceOf().getDate(), 
				"Other Contractor", 
				"SERVICES ",
				9,
				null));
		db.addLocationInfo(new LocationInfoHandler(0, 
				GlobalVariables.instanceOf().getDate(), 
				"933 08 82", 
				"CONTACT NO.",
				9,
				null));
		db.addLocation(new LocationHandler(10, GlobalVariables.instanceOf().getDate(), 14.634001, 121.107532, "Advance Food Concepts Mfg, Inc.", ConstantValue.ESTABLISHMENT,"service", 100));

		db.addLocationInfo(new LocationInfoHandler(0, 
				GlobalVariables.instanceOf().getDate(), 
				"Our Company is one of the suppliers  manufacturers of world class Dairy Liquid Mixes and list of Formulated product lines that services almost all the key food chains in the country today.Reorgarnized last February 2006 and now under the name Advance Food Concepts Manufacturing Inc. (AFCMI) - up to this day provides \"Quality Food Standard\" to the tapped Food Businesses in the market. Technological advancement, strict quality control and prompt deliveries of AFCMI products to its valued clientele are the key factors in maintaining good business relationship and loyalty among its food markets. At present, it is continuously exerting further effort in its product improvement, research & development and become a responsible leader in the industry.", 
				"BACKGROUND", 
				10,
				null));
		db.addLocationInfo(new LocationInfoHandler(0, 
				GlobalVariables.instanceOf().getDate(), 
				"Manufacturer � Food", 
				"SERVICES ",
				10,
				null));
		db.addLocationInfo(new LocationInfoHandler(0, 
				GlobalVariables.instanceOf().getDate(), 
				"646 97 08", 
				"CONTACT NO.",
				10,
				null));
		db.addLocation(new LocationHandler(11, GlobalVariables.instanceOf().getDate(), 14.636350, 121.088809, "Coats Manila Bay Inc.", ConstantValue.ESTABLISHMENT,"service", 100));

		db.addLocationInfo(new LocationInfoHandler(0, 
				GlobalVariables.instanceOf().getDate(), 
				"Coats Manila Bay, Inc. is part of the worldwide family of Coats companies founded nearly 250 years ago and is now based in over 60 countries. With a group focus on core business, Coats Manila Bay, Inc. is in the perfect position to lead the way forward in textile crafts, using a combination of brand strength, distribution, and technical expertise, along with a dedicated and knowledgeable team.", 
				"BACKGROUND", 
				11,
				null));
		db.addLocationInfo(new LocationInfoHandler(0, 
				GlobalVariables.instanceOf().getDate(), 
				"Manufacturer �  Garment/Textile", 
				"SERVICES ",
				11,
				null));
		db.addLocationInfo(new LocationInfoHandler(0, 
				GlobalVariables.instanceOf().getDate(), 
				"941 95 00", 
				"CONTACT NO.",
				11,
				null));
		db.addLocation(new LocationHandler(12, GlobalVariables.instanceOf().getDate(), 14.666228, 121.082522, "Basic Holdings Corporation", ConstantValue.ESTABLISHMENT,"service", 100));

		//		db.addLocationInfo(new LocationInfoHandler(0, 
		//				GlobalVariables.instanceOf().getDate(), 
		//				"Coats Manila Bay, Inc. is part of the worldwide family of Coats companies founded nearly 250 years ago and is now based in over 60 countries. With a group focus on core business, Coats Manila Bay, Inc. is in the perfect position to lead the way forward in textile crafts, using a combination of brand strength, distribution, and technical expertise, along with a dedicated and knowledgeable team.", 
		//				"BACKGROUND", 
		//				11,
		//				null));
		db.addLocationInfo(new LocationInfoHandler(0, 
				GlobalVariables.instanceOf().getDate(), 
				"Financial Inst. � Dealer in Sec", 
				"SERVICES ",
				12,
				null));
		//		db.addLocationInfo(new LocationInfoHandler(0, 
		//				GlobalVariables.instanceOf().getDate(), 
		//				"941 95 00", 
		//				"CONTACT NO.",
		//				12,
		//				null));
		db.addLocation(new LocationHandler(13, GlobalVariables.instanceOf().getDate(), 14.640320, 121.099688, "HAVi Logistics Phil., Inc.", ConstantValue.ESTABLISHMENT,"service", 100));
		db.addLocationInfo(new LocationInfoHandler(0, 
				GlobalVariables.instanceOf().getDate(), 
				"With over 26 years of experience, HAVI logistics has proven to be a world leader in the industry of combined logistics services for both food and non-food articles. Throughout the years, it has been providing extensive goods distribution services for quick-service restaurants, super and hypermarkets, and convenience retails in over 10,600 delivery points throughout all temperature zones around the world through its global network of more than 65 distribution centers. Being one of the four, fully functional divisions of HAVI Group LP�the others being HAVI Global Solutions, HAVI Food Services North America, and The Marketing Store�HAVI Logistics has helped its mother company expand its reach in a global scale, its unprecedented growth granting employment opportunities to more than 8,000 talented people in over 116 countries worldwide and making it comparable in size to the Forbes Top 20 Largest Companies.", 
				"BACKGROUND", 
				13,
				null));
		db.addLocationInfo(new LocationInfoHandler(0, 
				GlobalVariables.instanceOf().getDate(), 
				"Other Business", 
				"SERVICES ",
				13,
				null));
		db.addLocationInfo(new LocationInfoHandler(0, 
				GlobalVariables.instanceOf().getDate(), 
				"646 24 46", 
				"CONTACT NO.",
				13,
				null));

		db.addLocation(new LocationHandler(14, GlobalVariables.instanceOf().getDate(), 14.641317, 121.099001, "Steafano Footware corp.", ConstantValue.ESTABLISHMENT,"service", 100));
		db.addLocationInfo(new LocationInfoHandler(0, 
				GlobalVariables.instanceOf().getDate(), 
				"STEFANO FOOTWEAR CORPORATION is located in Marikina, Philippines. Company is working in General business business activities. Manufacture of other footwear, n.e.c.", 
				"BACKGROUND", 
				14,
				null));
		db.addLocationInfo(new LocationInfoHandler(0, 
				GlobalVariables.instanceOf().getDate(), 
				"Manufacturer � Shoes Men", 
				"SERVICES ",
				14,
				null));
		db.addLocationInfo(new LocationInfoHandler(0, 
				GlobalVariables.instanceOf().getDate(), 
				"941 21 88", 
				"CONTACT NO.",
				14,
				null));
		
		db.addLocation(new LocationHandler(15, GlobalVariables.instanceOf().getDate(), 14.633760, 121.079947, "SM Prime Holdings, Inc.", ConstantValue.ESTABLISHMENT,"service", 100));
		
		db.addLocationInfo(new LocationInfoHandler(0, 
				GlobalVariables.instanceOf().getDate(), 
				"SM Prime Holdings, inc. or SM Prime is the parent company of the SM Group's shopping malls. It is the largest shopping mall and retail operator in the Philippines. It was incorporated on 6 January 1994 to develop, conduct, operate and maintain the SM commercial shopping centres and all businesses related thereto, such as the lease of commercial spaces within the compound of shopping centres. It later went public on July 5, 1994 and subsequently grew to become the largest company listed on the Philippine Stock Exchange in terms of revenue. As of 2007, SM Prime Holdings became one of the largest shopping mall chains in the world. The company's main sources of revenues include rental income from mall and food courts, cinema ticket sales and amusement income from bowling and ice-skating", 
				"BACKGROUND", 
				15,
				null));
		db.addLocationInfo(new LocationInfoHandler(0, 
				GlobalVariables.instanceOf().getDate(), 
				"Lessor � Shopping Center", 
				"SERVICES ",
				15,
				null));
		db.addLocationInfo(new LocationInfoHandler(0, 
				GlobalVariables.instanceOf().getDate(), 
				"857 01 00", 
				"CONTACT NO.",
				15,
				null));
		
		db.addLocation(new LocationHandler(16, GlobalVariables.instanceOf().getDate(), 14.641732, 121.082693, "Puregold Price Club, Inc.", ConstantValue.ESTABLISHMENT,"service", 100));
		
		db.addLocationInfo(new LocationInfoHandler(0, 
				GlobalVariables.instanceOf().getDate(), 
				"Puregold Price Club, Inc. (PGOLD) is engaged in the business of buying, selling, distributing, and marketing wholesale or retail goods and commodities through out the Philippines. Incorporated in the year 1998, the Company conducts its operations through three retail formats and store brands. Hypermarkets, through \"Puregold Price Club\", offers a wide variety of general merchandise and a full-service supermarket with wholesale. Supermarkets, through \"Puregold Junior\", operates as a neighborhood store", 
				"BACKGROUND", 
				16,
				null));
		db.addLocationInfo(new LocationInfoHandler(0, 
				GlobalVariables.instanceOf().getDate(), 
				"Retailer � Supermarket", 
				"SERVICES ",
				16,
				null));
		db.addLocationInfo(new LocationInfoHandler(0, 
				GlobalVariables.instanceOf().getDate(), 
				"524 93 28", 
				"CONTACT NO.",
				16,
				null));
		
		db.addLocation(new LocationHandler(17, GlobalVariables.instanceOf().getDate(), 14.638410, 121.106039, "The Phil. Americal Life & Gen. Ins. Co.", ConstantValue.ESTABLISHMENT,"service", 100));
		db.addLocationInfo(new LocationInfoHandler(0, 
				GlobalVariables.instanceOf().getDate(), 
				"The Philippine American Life and General Insurance Company (also commonly known by its trade name, Philam Life) is an insurance company based in the Philippines. It is currently the largest life insurance company in the Philippines in terms of assets, net worth, investment and paid-up capital", 
				"BACKGROUND", 
				17,
				null));
		db.addLocationInfo(new LocationInfoHandler(0, 
				GlobalVariables.instanceOf().getDate(), 
				"Financial Inst. � Insurance Co.", 
				"SERVICES ",
				17,
				null));
		db.addLocationInfo(new LocationInfoHandler(0, 
				GlobalVariables.instanceOf().getDate(), 
				"474 87 73", 
				"CONTACT NO.",
				17,
				null));
		
		db.addLocation(new LocationHandler(18, GlobalVariables.instanceOf().getDate(), 14.638475, 121.111951, "AC Harris Cable Corporation", ConstantValue.ESTABLISHMENT,"service", 100));
		
		db.addLocationInfo(new LocationInfoHandler(0, 
				GlobalVariables.instanceOf().getDate(), 
				"AC HARRIS CABLE CORPORATION was incorporated on july 2004, With a group of dynamic individuals aspiring to provide quality Aluminum Cables for the quality conscious and service oriented customers.", 
				"BACKGROUND", 
				18,
				null));
		db.addLocationInfo(new LocationInfoHandler(0, 
				GlobalVariables.instanceOf().getDate(), 
				"Manufacturer � Electrical/Electronics", 
				"SERVICES ",
				18,
				null));
		db.addLocationInfo(new LocationInfoHandler(0, 
				GlobalVariables.instanceOf().getDate(), 
				"941 29 67", 
				"CONTACT NO.",
				18,
				null));
		
		db.addLocation(new LocationHandler(19, GlobalVariables.instanceOf().getDate(), 14.639778, 121.096942, "South Super Market Marikina. Br.", ConstantValue.ESTABLISHMENT,"service", 100));
		
//		db.addLocationInfo(new LocationInfoHandler(0, 
//				GlobalVariables.instanceOf().getDate(), 
//				"AC HARRIS CABLE CORPORATION was incorporated on july 2004, With a group of dynamic individuals aspiring to provide quality Aluminum Cables for the quality conscious and service oriented customers.", 
//				"BACKGROUND", 
//				19,
//				null));
		db.addLocationInfo(new LocationInfoHandler(0, 
				GlobalVariables.instanceOf().getDate(), 
				"Retailer � Supermarket", 
				"SERVICES ",
				19,
				null));
		db.addLocationInfo(new LocationInfoHandler(0, 
				GlobalVariables.instanceOf().getDate(), 
				"997 75 80", 
				"CONTACT NO.",
				19,
				null));
		db.addLocation(new LocationHandler(20, GlobalVariables.instanceOf().getDate(), 14.640358, 121.097157, "OSP Advantage System Corp.", ConstantValue.ESTABLISHMENT,"service", 100));
		db.addLocation(new LocationHandler(21, GlobalVariables.instanceOf().getDate(), 14.646367, 121.092488, "Architecks Metai Systems Inc.", ConstantValue.ESTABLISHMENT,"service", 100));
		db.addLocation(new LocationHandler(22, GlobalVariables.instanceOf().getDate(), 14.628917, 121.080310, "SM Appliance Center", ConstantValue.ESTABLISHMENT,"service", 100));
		//		/* amusement */
		db.addLocation(new LocationHandler(23, GlobalVariables.instanceOf().getDate(), 14.631847 ,121.082454, "KC WONDERLAND CORPORATION", ConstantValue.AMUSEMENT,"price", 100));
		db.addLocation(new LocationHandler(24, GlobalVariables.instanceOf().getDate(), 14.63505, 121.1013166, "GAME DIMENSION (ASIA METROTECH ELECTRONIC INC.)", ConstantValue.AMUSEMENT,"price", 100));
		db.addLocation(new LocationHandler(25, GlobalVariables.instanceOf().getDate(), 14.6429176, 121.1109042, "S & T LEISURE WORLWIDE, INC.", ConstantValue.AMUSEMENT,"price", 100));
		db.addLocation(new LocationHandler(26, GlobalVariables.instanceOf().getDate(), 14.639812, 121.109824, "KATIPUNAN PRIME BADMINTON CENTER", ConstantValue.AMUSEMENT,"price", 100));
		db.addLocation(new LocationHandler(27, GlobalVariables.instanceOf().getDate(), 14.647171, 121.11386, "TMH SPORTS HUB BAR & RESTO GRILL", ConstantValue.AMUSEMENT,"price", 100));
		db.addLocation(new LocationHandler(28, GlobalVariables.instanceOf().getDate(), 14.648753, 121.114689, "PLAYPRO TABLE TENNIS SPORTS HUB", ConstantValue.AMUSEMENT,"price", 100));
		db.addLocation(new LocationHandler(29, GlobalVariables.instanceOf().getDate(), 14.640197, 121.094277, "KA BARKADA KO CAFE BAR & GRILL", ConstantValue.AMUSEMENT,"price", 100));
		db.addLocation(new LocationHandler(30, GlobalVariables.instanceOf().getDate(), 14.636263, 121.095149, "WILD FOX RESTOBAR AND GRILL", ConstantValue.AMUSEMENT,"price", 100));
		db.addLocation(new LocationHandler(31, GlobalVariables.instanceOf().getDate(), 14.642918, 121.110904, "ALCO-HOLE BAR & GRILL", ConstantValue.AMUSEMENT,"price", 100));
		db.addLocation(new LocationHandler(32, GlobalVariables.instanceOf().getDate(), 14.648753, 121.114689, "TIL-TILAN GRILL", ConstantValue.AMUSEMENT,"price", 100));
		db.addLocation(new LocationHandler(33, GlobalVariables.instanceOf().getDate(), 14.642918, 121.110904, "JOBET'S GRILL RESTOBAR", ConstantValue.AMUSEMENT,"price", 100));
		db.addLocation(new LocationHandler(34, GlobalVariables.instanceOf().getDate(), 14.3896 , 121.5384, "SAINES BAR & GRILL", ConstantValue.AMUSEMENT,"price", 100));
		db.addLocation(new LocationHandler(35, GlobalVariables.instanceOf().getDate(), 14.63505, 121.101317, "ABSTRAKT GRILL & RESTOBAR", ConstantValue.AMUSEMENT,"price", 100));
		db.addLocation(new LocationHandler(36, GlobalVariables.instanceOf().getDate(), 14.640197, 121.094277, "KAPAYAPAAN BAR & GRILL", ConstantValue.AMUSEMENT,"price", 100));
		db.addLocation(new LocationHandler(37, GlobalVariables.instanceOf().getDate(), 14.3896, 121.5384, "PADI'S POINT RESTAURANT & BAR", ConstantValue.AMUSEMENT,"price", 100));
		db.addLocation(new LocationHandler(38, GlobalVariables.instanceOf().getDate(), 14.625702, 121.10237, "MARIKINA RIVER GOLD GRILL & RESTO BAR, INC.", ConstantValue.AMUSEMENT,"price", 100));
		db.addLocation(new LocationHandler(39, GlobalVariables.instanceOf().getDate(), 14.626901, 121.099429, "VAMECA FOODS INCORPORATED (TIYO'S)", ConstantValue.AMUSEMENT,"price", 100));
		db.addLocation(new LocationHandler(40, GlobalVariables.instanceOf().getDate(), 14.626901, 121.099429, "STAR PALACE CLUB", ConstantValue.AMUSEMENT,"price", 100));
		db.addLocation(new LocationHandler(41, GlobalVariables.instanceOf().getDate(), 14.631848, 121.082454, "BONFIRE GRILL", ConstantValue.AMUSEMENT,"price", 100));
		db.addLocation(new LocationHandler(42, GlobalVariables.instanceOf().getDate(), 14.647543, 121.126963, "BEACH HOUSE IHAWAN INC.", ConstantValue.AMUSEMENT,"price", 100));
		db.addLocation(new LocationHandler(43, GlobalVariables.instanceOf().getDate(), 14.640197, 121.094277, "MINUM GRILL", ConstantValue.AMUSEMENT,"price", 100));
		db.addLocation(new LocationHandler(44, GlobalVariables.instanceOf().getDate(), 14.647171, 121.11386, "LET IT BE Q GRILL", ConstantValue.AMUSEMENT,"price", 100));
		db.addLocation(new LocationHandler(45, GlobalVariables.instanceOf().getDate(), 14.3896 , 121.5384, "BORAFIRE GRILL & RESTAURANT", ConstantValue.AMUSEMENT,"price", 100));
		db.addLocation(new LocationHandler(46, GlobalVariables.instanceOf().getDate(), 14.3896, 121.5384, "RIVER ROCK", ConstantValue.AMUSEMENT,"price", 100));
		db.addLocation(new LocationHandler(47, GlobalVariables.instanceOf().getDate(), 14.647171, 121.11386, "YUINA'S RESTO BAR", ConstantValue.AMUSEMENT,"price", 100));
		db.addLocation(new LocationHandler(48, GlobalVariables.instanceOf().getDate(), 14.656684, 121.10352, "ATCHA'S GRILL", ConstantValue.AMUSEMENT,"price", 100));
		db.addLocation(new LocationHandler(49, GlobalVariables.instanceOf().getDate(), 14.3896, 121.5384, "PEKKABU BAR AND RESTAURANT", ConstantValue.AMUSEMENT,"price", 100));
		db.addLocation(new LocationHandler(50, GlobalVariables.instanceOf().getDate(), 14.647171, 121.11386, "1ST STREET CAFE BAR", ConstantValue.AMUSEMENT,"price", 100));
		db.addLocation(new LocationHandler(51, GlobalVariables.instanceOf().getDate(), 14.639812, 121.109824, "BOSS JUN'S MUSIC PLACE", ConstantValue.AMUSEMENT,"price", 100));
		db.addLocation(new LocationHandler(52, GlobalVariables.instanceOf().getDate(), 14.639812, 121.109824, "FOX'IES RESTOBAR", ConstantValue.AMUSEMENT,"price", 100));
		db.addLocation(new LocationHandler(53, GlobalVariables.instanceOf().getDate(), 14.3896, 121.5384, "J.E.K. BAR & GRILL CO.", ConstantValue.AMUSEMENT,"price", 100));
		db.addLocation(new LocationHandler(54, GlobalVariables.instanceOf().getDate(), 14.640197, 121.094277, "SURIGAO BAR & KITCHENETTE", ConstantValue.AMUSEMENT,"price", 100));
		db.addLocation(new LocationHandler(55, GlobalVariables.instanceOf().getDate(), 14.683426, 121.109615, "NAMAE RESTO BAR", ConstantValue.AMUSEMENT,"price", 100));
		db.addLocation(new LocationHandler(56, GlobalVariables.instanceOf().getDate(), 14.654531, 121.099561, "AIYANNA 2 RESTAURANT & KTV BAR", ConstantValue.AMUSEMENT,"price", 100));
		db.addLocation(new LocationHandler(57, GlobalVariables.instanceOf().getDate(), 14.649851, 121.114178, "GOO LAGOON BAR AND RESTAURANT", ConstantValue.AMUSEMENT,"price", 100));
		db.addLocation(new LocationHandler(58, GlobalVariables.instanceOf().getDate(), 14.666494, 121.117772, "SAI STEAK HAUS & KTV BAR", ConstantValue.AMUSEMENT,"price", 100));
		db.addLocation(new LocationHandler(59, GlobalVariables.instanceOf().getDate(), 14.642574,121.095789, "VALDENOR'S DINER AND BAR", ConstantValue.AMUSEMENT,"price", 100));
		db.addLocation(new LocationHandler(60, GlobalVariables.instanceOf().getDate(), 14.3896, 121.5384, "PULOT GATA", ConstantValue.AMUSEMENT,"price", 100));
		db.addLocation(new LocationHandler(61, GlobalVariables.instanceOf().getDate(), 14.632968 ,121.094832, "DAMMS RESTOBAR", ConstantValue.AMUSEMENT,"price", 100));
		db.addLocation(new LocationHandler(62, GlobalVariables.instanceOf().getDate(), 14.649851, 121.114178, "WHARF 124 BAR AND RESTO", ConstantValue.AMUSEMENT,"price", 100));
		db.addLocation(new LocationHandler(63, GlobalVariables.instanceOf().getDate(), 14.3896, 121.5384, "LOMI KING FOOD HAUS", ConstantValue.AMUSEMENT,"price", 100));
		db.addLocation(new LocationHandler(64, GlobalVariables.instanceOf().getDate(), 14.626901, 121.099429, "CALIS GRILL & BISTRO", ConstantValue.AMUSEMENT,"price", 100));
		db.addLocation(new LocationHandler(65, GlobalVariables.instanceOf().getDate(), 14.637462, 121.095951, "KA BERK'S RESTO BAR", ConstantValue.AMUSEMENT,"price", 100));
		db.addLocation(new LocationHandler(66, GlobalVariables.instanceOf().getDate(), 14.637817, 121.094138, "HPDHON RESTOBAR", ConstantValue.AMUSEMENT,"price", 100));
		db.addLocation(new LocationHandler(67, GlobalVariables.instanceOf().getDate(), 12.879721, 121.774017, "METRO YHOLKS RESTOBAR AND BILLIARDS", ConstantValue.AMUSEMENT,"price", 100));
		db.addLocation(new LocationHandler(68, GlobalVariables.instanceOf().getDate(),14.649851, 121.114178, "EDIL VAL RESTOBAR", ConstantValue.AMUSEMENT,"price", 100));
		db.addLocation(new LocationHandler(69, GlobalVariables.instanceOf().getDate(), 14.669863, 121.108369, "THE GOLDEN BAR KTV & EATERY", ConstantValue.AMUSEMENT,"price", 100));
		db.addLocation(new LocationHandler(70, GlobalVariables.instanceOf().getDate(), 14.625702, 121.10237, "BONE HAPINESS", ConstantValue.AMUSEMENT,"price", 100));
		db.addLocation(new LocationHandler(71, GlobalVariables.instanceOf().getDate(), 14.647213, 121.112573, "JHEPOII'S PLACE KTV AND RESTOBAR", ConstantValue.AMUSEMENT,"price", 100));
		db.addLocation(new LocationHandler(72, GlobalVariables.instanceOf().getDate(), 14.63432, 121.094374, "ATTITUDE BAR & RESTO", ConstantValue.AMUSEMENT,"price", 100));
		db.addLocation(new LocationHandler(73, GlobalVariables.instanceOf().getDate(), 14.647213,121.112573, "FLAMINGOES GARDEN RESORT", ConstantValue.AMUSEMENT,"price", 100));
		db.addLocation(new LocationHandler(74, GlobalVariables.instanceOf().getDate(), 14.638804, 121.110944, "WATER NYMPH RESORT", ConstantValue.AMUSEMENT,"price", 100));
		db.addLocation(new LocationHandler(75, GlobalVariables.instanceOf().getDate(), 14.651896 ,121.122118, "VILLA RONAR GARDEN RESORT INCORPORATED", ConstantValue.AMUSEMENT,"price", 100));
		db.addLocation(new LocationHandler(76, GlobalVariables.instanceOf().getDate(), 14.628568,121.110217, "JB'S STAR CIRCLE GARDEN & RESORT", ConstantValue.AMUSEMENT,"price", 100));
		db.addLocation(new LocationHandler(77, GlobalVariables.instanceOf().getDate(), 14.647213, 121.112573, "MCS GARDEN RESORT", ConstantValue.AMUSEMENT,"price", 100));
		db.addLocation(new LocationHandler(78, GlobalVariables.instanceOf().getDate(), 14.658875, 121.128078, "VISTA AL CIUDAD RESORT", ConstantValue.AMUSEMENT,"price", 100));
		db.addLocation(new LocationHandler(79, GlobalVariables.instanceOf().getDate(), 14.658875, 121.128078, "ARMSCOR SHOOTING RANGES, INCORPORATED", ConstantValue.AMUSEMENT,"price", 100));
		db.addLocation(new LocationHandler(80, GlobalVariables.instanceOf().getDate(), 14.641689, 121.111646, "LA STANZA MEETINGS & LIFESTYLE VENUE", ConstantValue.AMUSEMENT,"price", 100));
		db.addLocation(new LocationHandler(81, GlobalVariables.instanceOf().getDate(), 14.646744, 121.105159, "SERBALL FAMILY SWIMMING POOL", ConstantValue.AMUSEMENT,"price", 100));
		/* Banks */
		db.addLocation(new LocationHandler(82, GlobalVariables.instanceOf().getDate(), 14.626901, 121.099429, "METROPOLITAN BANK & TRUST CO. (SAN ROQUE)", ConstantValue.BANK,"cash", 100));
		db.addLocation(new LocationHandler(83, GlobalVariables.instanceOf().getDate(), 14.647213, 121.112573, "BPI FAMILY SAVINGS BANK", ConstantValue.BANK,"cash", 100));
		db.addLocation(new LocationHandler(84, GlobalVariables.instanceOf().getDate(), 14.625702, 121.10237, "RIZAL COMMERCIAL BANKING CORPORATION", ConstantValue.BANK,"cash", 100));
		db.addLocation(new LocationHandler(85, GlobalVariables.instanceOf().getDate(), 14.650652,121.106947, "PHILIPPINE NATIONAL BANK", ConstantValue.BANK,"cash", 100));
		db.addLocation(new LocationHandler(86, GlobalVariables.instanceOf().getDate(), 14.63292, 121.097725, "PHILIPPINE NATIONAL BANK (MARIKINA BRANCH)", ConstantValue.BANK,"cash", 100));
		db.addLocation(new LocationHandler(87, GlobalVariables.instanceOf().getDate(),14.625702, 121.10237, "UNION BANK OF THE PHILIPPINES (MARIKINA BRANCH)", ConstantValue.BANK,"cash", 100));
		db.addLocation(new LocationHandler(88, GlobalVariables.instanceOf().getDate(), 14.659323, 121.113383, "METROPOLITAN BANK & TRUST CO. ( PARANG )", ConstantValue.BANK,"cash", 100));
		db.addLocation(new LocationHandler(89, GlobalVariables.instanceOf().getDate(), 14.640197, 121.094277, "PHILIPPINE NATIONAL BANK", ConstantValue.BANK,"cash", 100));
		db.addLocation(new LocationHandler(90, GlobalVariables.instanceOf().getDate(), 14.650652, 121.106947, "SECURITY BANK CORPORATION", ConstantValue.BANK,"cash", 100));
		db.addLocation(new LocationHandler(91, GlobalVariables.instanceOf().getDate(), 14.650652, 121.106947, "UNITED COCONUT PLANTERS BANK", ConstantValue.BANK,"cash", 100));
		db.addLocation(new LocationHandler(92, GlobalVariables.instanceOf().getDate(), 14.626901,121.099429, "PHILIPPINE NATIONAL BANK", ConstantValue.BANK,"cash", 100));
		db.addLocation(new LocationHandler(93, GlobalVariables.instanceOf().getDate(), 14.650652, 121.106947, "BANK OF THE PHILIPPINE ISLANDS - MARIKINA BRANCH", ConstantValue.BANK,"cash", 100));
		db.addLocation(new LocationHandler(94, GlobalVariables.instanceOf().getDate(), 14.649325 ,121.10108, "BPI FAMILY SAVINGS BANK - MARIKINA STA. ELENA BRANCH", ConstantValue.BANK,"cash", 100));
		db.addLocation(new LocationHandler(95, GlobalVariables.instanceOf().getDate(), 14.649325,121.10108, "METROPOLITAN BANK & TRUST CO.", ConstantValue.BANK,"cash", 100));
		db.addLocation(new LocationHandler(96, GlobalVariables.instanceOf().getDate(), 14.625702, 121.10237, "BANK OF COMMERCE", ConstantValue.BANK,"cash", 100));
		db.addLocation(new LocationHandler(97, GlobalVariables.instanceOf().getDate(), 14.649325, 121.10108, "BANCO DE ORO UNIBANK, INC.", ConstantValue.BANK,"cash", 100));
		db.addLocation(new LocationHandler(98, GlobalVariables.instanceOf().getDate(), 14.650652, 121.106947, "UNITED COCONUT PLANTERS BANK", ConstantValue.BANK,"cash", 100));
		db.addLocation(new LocationHandler(99, GlobalVariables.instanceOf().getDate(), 14.635459, 121.10113, "BANK OF COMMERCE", ConstantValue.BANK,"cash", 100));
		db.addLocation(new LocationHandler(100, GlobalVariables.instanceOf().getDate(), 14.650652, 121.106947, "PHILIPPINE BANK OF COMMUNICATIONS", ConstantValue.BANK,"cash", 100));


		db.addLocation(new LocationHandler(101, GlobalVariables.instanceOf().getDate(), 14.626901, 121.099429, "METROPOLITAN BANK & TRUST CO. (SAN ROQUE)", ConstantValue.BANK,"cash", 100));
		db.addLocation(new LocationHandler(102, GlobalVariables.instanceOf().getDate(), 14.647213, 121.112573, "BPI FAMILY SAVINGS BANK", ConstantValue.BANK,"cash", 100));
		db.addLocation(new LocationHandler(103, GlobalVariables.instanceOf().getDate(), 14.625702, 121.10237, "RIZAL COMMERCIAL BANKING CORPORATION", ConstantValue.BANK,"cash", 100));
		db.addLocation(new LocationHandler(104, GlobalVariables.instanceOf().getDate(), 14.650652,121.106947, "PHILIPPINE NATIONAL BANK", ConstantValue.BANK,"cash", 100));
		db.addLocation(new LocationHandler(105, GlobalVariables.instanceOf().getDate(), 14.63292, 121.097725, "PHILIPPINE NATIONAL BANK (MARIKINA BRANCH)", ConstantValue.BANK,"cash", 100));
		db.addLocation(new LocationHandler(106, GlobalVariables.instanceOf().getDate(),14.625702, 121.10237, "UNION BANK OF THE PHILIPPINES (MARIKINA BRANCH)", ConstantValue.BANK,"cash", 100));
		db.addLocation(new LocationHandler(107, GlobalVariables.instanceOf().getDate(), 14.659323, 121.113383, "METROPOLITAN BANK & TRUST CO. ( PARANG )", ConstantValue.BANK,"cash", 100));
		db.addLocation(new LocationHandler(108, GlobalVariables.instanceOf().getDate(), 14.640197, 121.094277, "PHILIPPINE NATIONAL BANK", ConstantValue.BANK,"cash", 100));
		db.addLocation(new LocationHandler(109, GlobalVariables.instanceOf().getDate(), 14.650652, 121.106947, "SECURITY BANK CORPORATION", ConstantValue.BANK,"cash", 100));
		db.addLocation(new LocationHandler(110, GlobalVariables.instanceOf().getDate(), 14.650652, 121.106947, "UNITED COCONUT PLANTERS BANK", ConstantValue.BANK,"cash", 100));
		db.addLocation(new LocationHandler(111, GlobalVariables.instanceOf().getDate(), 14.626901,121.099429, "PHILIPPINE NATIONAL BANK", ConstantValue.BANK,"cash", 100));
		db.addLocation(new LocationHandler(112, GlobalVariables.instanceOf().getDate(), 14.650652, 121.106947, "BANK OF THE PHILIPPINE ISLANDS - MARIKINA BRANCH", ConstantValue.BANK,"cash", 100));
		db.addLocation(new LocationHandler(113, GlobalVariables.instanceOf().getDate(), 14.649325 ,121.10108, "BPI FAMILY SAVINGS BANK - MARIKINA STA. ELENA BRANCH", ConstantValue.BANK,"cash", 100));
		db.addLocation(new LocationHandler(114, GlobalVariables.instanceOf().getDate(), 14.649325,121.10108, "METROPOLITAN BANK & TRUST CO.", ConstantValue.BANK,"cash", 100));
		db.addLocation(new LocationHandler(115, GlobalVariables.instanceOf().getDate(), 14.625702, 121.10237, "BANK OF COMMERCE", ConstantValue.BANK,"cash", 100));
		db.addLocation(new LocationHandler(116, GlobalVariables.instanceOf().getDate(), 14.649325, 121.10108, "BANCO DE ORO UNIBANK, INC.", ConstantValue.BANK,"cash", 100));
		db.addLocation(new LocationHandler(117, GlobalVariables.instanceOf().getDate(), 14.650652, 121.106947, "UNITED COCONUT PLANTERS BANK", ConstantValue.BANK,"cash", 100));
		db.addLocation(new LocationHandler(118, GlobalVariables.instanceOf().getDate(), 14.635459, 121.10113, "BANK OF COMMERCE", ConstantValue.BANK,"cash", 100));
		db.addLocation(new LocationHandler(119, GlobalVariables.instanceOf().getDate(), 14.650652, 121.106947, "PHILIPPINE BANK OF COMMUNICATIONS", ConstantValue.BANK,"cash", 100));
		db.addLocation(new LocationHandler(120, GlobalVariables.instanceOf().getDate(), 14.632453, 121.095195, "BANK OF COMMERCE", ConstantValue.BANK,"cash", 100));
		db.addLocation(new LocationHandler(121, GlobalVariables.instanceOf().getDate(), 14.632711, 121.096491, "BANK OF THE PHILIPPINE ISLANDS", ConstantValue.BANK,"cash", 100));
		db.addLocation(new LocationHandler(122, GlobalVariables.instanceOf().getDate(),14.626901, 121.099429, "BPI MARIKINA GIL FERNANDO AVE. BRANCH", ConstantValue.BANK,"cash", 100));
		db.addLocation(new LocationHandler(123, GlobalVariables.instanceOf().getDate(), 14.650652,121.106947, "BDO UNIBANK, INC.", ConstantValue.BANK,"cash", 100));
		db.addLocation(new LocationHandler(124, GlobalVariables.instanceOf().getDate(), 14.632453, 121.095195, "CHINA BANKING CORPORATION", ConstantValue.BANK,"cash", 100));
		db.addLocation(new LocationHandler(125, GlobalVariables.instanceOf().getDate(),14.631848, 121.082454, "RCBC SAVINGS BANK, INC.", ConstantValue.BANK,"cash", 100));
		db.addLocation(new LocationHandler(126, GlobalVariables.instanceOf().getDate(), 14.63505, 121.101317, "METROPOLITAN BANK & TRUST CO.", ConstantValue.BANK,"cash", 100));
		db.addLocation(new LocationHandler(127, GlobalVariables.instanceOf().getDate(), 14.625702, 121.10237, "DEVELOPMENT BANK OF THE PHILIPPINES", ConstantValue.BANK,"cash", 100));
		db.addLocation(new LocationHandler(128, GlobalVariables.instanceOf().getDate(), 14.642718, 121.094989, "LAND BANK OF THE PHILIPPINES- MARIKINA", ConstantValue.BANK,"cash", 100));
		db.addLocation(new LocationHandler(129, GlobalVariables.instanceOf().getDate(), 14.650652, 121.106947, "RIZAL COMMERCIAL BANKING CORPORATION", ConstantValue.BANK,"cash", 100));
		db.addLocation(new LocationHandler(130, GlobalVariables.instanceOf().getDate(), 14.626901,121.099429, "PHILIPPINE VETERANS BANK", ConstantValue.BANK,"cash", 100));
		db.addLocation(new LocationHandler(131, GlobalVariables.instanceOf().getDate(), 14.635377, 121.09889, "BANK OF THE PHILIPPINES ISLAND", ConstantValue.BANK,"cash", 100));
		db.addLocation(new LocationHandler(132, GlobalVariables.instanceOf().getDate(), 14.627144 ,121.097756, "CTBC BANK (PHILIPPINES) CORP.", ConstantValue.BANK,"cash", 100));
		db.addLocation(new LocationHandler(133, GlobalVariables.instanceOf().getDate(), 14.627749,121.102155, "BANCO DE ORO - G. FERNANDO BRANCH", ConstantValue.BANK,"cash", 100));
		db.addLocation(new LocationHandler(134, GlobalVariables.instanceOf().getDate(), 14.6339, 121.085759, "METROPOLITAN BANK & TRUST CO.", ConstantValue.BANK,"cash", 100));
		db.addLocation(new LocationHandler(135, GlobalVariables.instanceOf().getDate(), 14.631848, 121.082454, "PHILIPPINE SAVINGS BANK", ConstantValue.BANK,"cash", 100));
		db.addLocation(new LocationHandler(136, GlobalVariables.instanceOf().getDate(), 14.621458, 121.102091, "LAND BANK OF THE PHILIPPINES", ConstantValue.BANK,"cash", 100));
		db.addLocation(new LocationHandler(137, GlobalVariables.instanceOf().getDate(), 14.626901, 121.099429, "STERLING BANK OF ASIA - MARIKINA BRANCH", ConstantValue.BANK,"cash", 100));
		db.addLocation(new LocationHandler(138, GlobalVariables.instanceOf().getDate(),14.659323, 121.113383, "PHILIPPINE NATIONAL BANK", ConstantValue.BANK,"cash", 100));
		db.addLocation(new LocationHandler(139, GlobalVariables.instanceOf().getDate(), 14.624922, 121.086535, "METROPOLITAN BANK & TRUST CO.", ConstantValue.BANK,"cash", 100));
		db.addLocation(new LocationHandler(140, GlobalVariables.instanceOf().getDate(), 14.650652, 121.106947, "MAYBANK PHILIPPINES, INCORPORATED BRANCH", ConstantValue.BANK,"cash", 100));
		db.addLocation(new LocationHandler(141, GlobalVariables.instanceOf().getDate(),14.650652, 121.106947, "METROPOLITAN BANK & TRUST CO.", ConstantValue.BANK,"cash", 100));
		db.addLocation(new LocationHandler(142, GlobalVariables.instanceOf().getDate(), 14.640197,121.094277, "CHINA BANKING CORPORATION - CONCEPCION I BRANCH", ConstantValue.BANK,"cash", 100));
		db.addLocation(new LocationHandler(143, GlobalVariables.instanceOf().getDate(), 14.625261,121.083483, "BANCO DE ORO UNIBANK, INC. - SM CITY MARIKINA", ConstantValue.BANK,"cash", 100));
		db.addLocation(new LocationHandler(144, GlobalVariables.instanceOf().getDate(),14.650652, 121.106947, "ROBINSONS BANK CORPORATION", ConstantValue.BANK,"cash", 100));
		db.addLocation(new LocationHandler(145, GlobalVariables.instanceOf().getDate(), 14.640539, 121.120917, "CHINA BANKING", ConstantValue.BANK,"cash", 100));
		db.addLocation(new LocationHandler(146, GlobalVariables.instanceOf().getDate(), 14.635514, 121.096233, "BANCO DE ORO UNIBANK, INC.", ConstantValue.BANK,"cash", 100));
		db.addLocation(new LocationHandler(147, GlobalVariables.instanceOf().getDate(), 14.669907, 121.108947, "BDO UNIBANK, INC. (NANGKA BRANCH)", ConstantValue.BANK,"cash", 100));
		db.addLocation(new LocationHandler(148, GlobalVariables.instanceOf().getDate(), 14.625261, 121.083483, "CHINA BANKING CORPORATION - SM MARIKINA BRANCH", ConstantValue.BANK,"cash", 100));
		db.addLocation(new LocationHandler(149, GlobalVariables.instanceOf().getDate(), 14.645309,121.096364, "BANCO DE ORO UNIBANK, INC. (BRANCH)", ConstantValue.BANK,"cash", 100));
		db.addLocation(new LocationHandler(150, GlobalVariables.instanceOf().getDate(), 14.622819, 121.091255, "BDO UNIBANK, INC. (MARIKINA - KALUMPANG BRANCH)", ConstantValue.BANK,"cash", 100));
		db.addLocation(new LocationHandler(151, GlobalVariables.instanceOf().getDate(), 14.650652 ,121.106947, "ASIA UNITED BANK CORPORATION", ConstantValue.BANK,"cash", 100));
		db.addLocation(new LocationHandler(152, GlobalVariables.instanceOf().getDate(), 14.660257,121.113002, "BANCO DE ORO UNIBANK, INC.", ConstantValue.BANK,"cash", 100));
		db.addLocation(new LocationHandler(153, GlobalVariables.instanceOf().getDate(), 14.669863, 121.108369, "METROPOLITAN BANK & TRUST CO.", ConstantValue.BANK,"cash", 100));
		db.addLocation(new LocationHandler(154, GlobalVariables.instanceOf().getDate(), 14.626901, 121.099429, "EAST WEST BANKING CORPORATION", ConstantValue.BANK,"cash", 100));
		db.addLocation(new LocationHandler(155, GlobalVariables.instanceOf().getDate(), 14.6339, 121.085759, "BDO UNIBANK, INC.", ConstantValue.BANK,"cash", 100));
		db.addLocation(new LocationHandler(156, GlobalVariables.instanceOf().getDate(), 14.631297, 121.101633, "CHINA BANKING CORPORATION (GIL FERNANDO BRANCH)", ConstantValue.BANK,"cash", 100));
		db.addLocation(new LocationHandler(157, GlobalVariables.instanceOf().getDate(),14.626901, 121.099429, "ASIA UNITED BANK", ConstantValue.BANK,"cash", 100));


		db.addLocation(new LocationHandler(158, GlobalVariables.instanceOf().getDate(), 14.650652, 121.106947, "EAST WEST BANKING CORPORATION", ConstantValue.BANK,"cash", 100));
		db.addLocation(new LocationHandler(159, GlobalVariables.instanceOf().getDate(),14.640197, 121.094277, "EAST WEST BANKING CORPORATION", ConstantValue.BANK,"cash", 100));
		db.addLocation(new LocationHandler(160, GlobalVariables.instanceOf().getDate(), 14.659323, 121.113383, "EAST WEST BANKING CORPORATION", ConstantValue.BANK,"cash", 100));
		db.addLocation(new LocationHandler(161, GlobalVariables.instanceOf().getDate(), 14.642033, 121.11076, "BDO UNIBANK, INC. (MARIKINA KATIPUNAN BRANCH)", ConstantValue.BANK,"cash", 100));
		db.addLocation(new LocationHandler(162, GlobalVariables.instanceOf().getDate(),14.625008, 121.100246, "SECURITY BANK CORPORATION", ConstantValue.BANK,"cash", 100));
		db.addLocation(new LocationHandler(163, GlobalVariables.instanceOf().getDate(), 14.640197, 121.094277, "MARIKINA VALLEY SAVINGS BANK, INC.", ConstantValue.BANK,"cash", 100));
		db.addLocation(new LocationHandler(164, GlobalVariables.instanceOf().getDate(), 14.640197,121.094277, "EASTERN RIZAL (JALA JALA) RURAL BANK, INC", ConstantValue.BANK,"cash", 100));
		db.addLocation(new LocationHandler(165, GlobalVariables.instanceOf().getDate(),14.639209, 121.094302, "COUNTRY BUILDERS BANK, INC. (A RURAL BANK)", ConstantValue.BANK,"cash", 100));
		db.addLocation(new LocationHandler(166, GlobalVariables.instanceOf().getDate(),14.649486, 121.10207, "MVSM BANK (A RURAL BANK SINCE 1953), INC.", ConstantValue.BANK,"cash", 100));
		db.addLocation(new LocationHandler(167, GlobalVariables.instanceOf().getDate(), 14.620897, 121.102563, "PHILIPPINE SAVINGS BANK.", ConstantValue.BANK,"cash", 100));
		db.addLocation(new LocationHandler(168, GlobalVariables.instanceOf().getDate(), 14.650652,121.106947, "PHILIPPINE SAVINGS BANK", ConstantValue.BANK,"cash", 100));
		db.addLocation(new LocationHandler(169, GlobalVariables.instanceOf().getDate(), 14.640197, 121.094277, "RCBC SAVINGS BANK- SAN ROQUE BRANCH", ConstantValue.BANK,"cash", 100));
		db.addLocation(new LocationHandler(170, GlobalVariables.instanceOf().getDate(), 14.649465, 121.10207, "MALAYAN BANK SAVINGS AND MORTGAGE BANK", ConstantValue.BANK,"cash", 100));
		db.addLocation(new LocationHandler(171, GlobalVariables.instanceOf().getDate(), 14.620665, 121.102057, "LIFE SAVINGS BANK, INC.", ConstantValue.BANK,"cash", 100));
		db.addLocation(new LocationHandler(172, GlobalVariables.instanceOf().getDate(), 14.635452 , 121.096898, "PHILIPPINE SAVINGS BANK", ConstantValue.BANK,"cash", 100));
		db.addLocation(new LocationHandler(173, GlobalVariables.instanceOf().getDate(), 14.625702,121.10237, "PLANTERS DEVELOPMENT BANK", ConstantValue.BANK,"cash", 100));
		db.addLocation(new LocationHandler(174, GlobalVariables.instanceOf().getDate(), 14.631848, 121.082454, "BPI RIVERBANKS BRANCH", ConstantValue.BANK,"cash", 100));
		db.addLocation(new LocationHandler(175, GlobalVariables.instanceOf().getDate(), 14.640197,121.094277, "PHILIPPINE BUSINESS BANK BRANCH", ConstantValue.BANK,"cash", 100));
		db.addLocation(new LocationHandler(176, GlobalVariables.instanceOf().getDate(), 14.650652, 121.106947, "PHILIPPINE BUSINESS BANK, INC - CONCEPCION BRANCH", ConstantValue.BANK,"cash", 100));
		db.addLocation(new LocationHandler(177, GlobalVariables.instanceOf().getDate(),14.634829, 121.101962, "PHILIPPINE SAVINGS BANK", ConstantValue.BANK,"cash", 100));
		db.addLocation(new LocationHandler(178, GlobalVariables.instanceOf().getDate(), 14.645309, 121.096364, "PHILIPPINE SAVINGS BANK - ATM", ConstantValue.BANK,"cash", 100));
		db.addLocation(new LocationHandler(179, GlobalVariables.instanceOf().getDate(), 14.659323, 121.113383, "PHILIPPINE SAVINGS BANK", ConstantValue.BANK,"cash", 100));
		db.addLocation(new LocationHandler(180, GlobalVariables.instanceOf().getDate(), 14.631297, 121.101633, "CITY SAVINGS BANK, INC.", ConstantValue.BANK,"cash", 100));
		db.addLocation(new LocationHandler(181, GlobalVariables.instanceOf().getDate(),14.640197, 121.094277, "SECURITY BANK SAVINGS CORPORATION", ConstantValue.BANK,"cash", 100));
		db.addLocation(new LocationHandler(182, GlobalVariables.instanceOf().getDate(), 14.650652, 121.106947, "CHINA BANK SAVINGS, INCORPORATION MARIKINA BRANCH", ConstantValue.BANK,"cash", 100));
		db.addLocation(new LocationHandler(183, GlobalVariables.instanceOf().getDate(),14.650652, 121.106947, "BANKONE SAVINGS AND TRUST CORPORATION", ConstantValue.BANK,"cash", 100));
		db.addLocation(new LocationHandler(184, GlobalVariables.instanceOf().getDate(), 14.642407, 121.094667, "LUZON DEVELOPMENT BANK", ConstantValue.BANK,"cash", 100));
		//whole sale
		db.addLocation(new LocationHandler(185, GlobalVariables.instanceOf().getDate(), 14.636263, 121.095149, "TUPPERWARE BRANDS PHILIPPINES, INC.", ConstantValue.MALL,"sale", 100));
		db.addLocation(new LocationHandler(186, GlobalVariables.instanceOf().getDate(), 14.583564,121.179009, "TUPPERWARE BRANDS PHILIPPINES, INC.", ConstantValue.MALL,"sale", 100));
		db.addLocation(new LocationHandler(187, GlobalVariables.instanceOf().getDate(), 14.599672, 121.075147, "KEMISTAR CORPORATION", ConstantValue.MALL,"sale", 100));
		db.addLocation(new LocationHandler(188, GlobalVariables.instanceOf().getDate(), 14.631297, 121.101633, "PILIPINAS TAJ AUTOGROUP INC.", ConstantValue.MALL,"sale", 100));
		db.addLocation(new LocationHandler(189, GlobalVariables.instanceOf().getDate(), 14.620665, 121.102057, "SPLASH CORPORATION", ConstantValue.MALL,"sale", 100));
		db.addLocation(new LocationHandler(190, GlobalVariables.instanceOf().getDate(), 14.645309, 121.096364, "THRU AIR SYSTEMS SPECIALIST INC.", ConstantValue.MALL,"sale", 100));
		db.addLocation(new LocationHandler(191, GlobalVariables.instanceOf().getDate(), 14.626388, 121.078155, "REQUISIMO DAIRY MARKETING INCORPORATED", ConstantValue.MALL,"sale", 100));
		db.addLocation(new LocationHandler(192, GlobalVariables.instanceOf().getDate(),14.620705, 121.100703, "HANSEN ENTERPRISES", ConstantValue.MALL,"sale", 100));
		db.addLocation(new LocationHandler(193, GlobalVariables.instanceOf().getDate(), 14.645234, 121.103228, "PHILIPPINE SPRING WATER RESOURCES, INC.", ConstantValue.MALL,"sale", 100));
		db.addLocation(new LocationHandler(194, GlobalVariables.instanceOf().getDate(),14.629146, 121.100055, "RADMAR PHARMA CORPORATION", ConstantValue.MALL,"sale", 100));
		db.addLocation(new LocationHandler(195, GlobalVariables.instanceOf().getDate(), 14.598559, 121.073578, "LUNARMED PHARMA TRADING", ConstantValue.MALL,"sale", 100));
		db.addLocation(new LocationHandler(196, GlobalVariables.instanceOf().getDate(), 14.647213, 121.112573, "BIOSERA INC.", ConstantValue.MALL,"sale", 100));
		db.addLocation(new LocationHandler(197, GlobalVariables.instanceOf().getDate(), 14.620045,121.097672, "PENTALAB PHARMA CORPORATION", ConstantValue.MALL,"sale", 100));
		db.addLocation(new LocationHandler(198, GlobalVariables.instanceOf().getDate(), 14.620665, 121.102057, "CCA COFFEE VENDING MACHINE", ConstantValue.MALL,"sale", 100));
		db.addLocation(new LocationHandler(199, GlobalVariables.instanceOf().getDate(), 14.645309, 121.096364, "SWEET CZY PIZZA PIE STATION", ConstantValue.MALL,"sale", 100));
		db.addLocation(new LocationHandler(200, GlobalVariables.instanceOf().getDate(), 14.635379, 121.082861, "LIVEGREEN PROJECT INC.", ConstantValue.MALL,"sale", 100));
		db.addLocation(new LocationHandler(201, GlobalVariables.instanceOf().getDate(), 14.642944, 121.107757, "STAR ALLIANCE SHOES TRADING CORPORATION", ConstantValue.MALL,"sale", 100));
		db.addLocation(new LocationHandler(202, GlobalVariables.instanceOf().getDate(), 14.655071, 121.106709, "ARVIN CARE PHARMA INC.", ConstantValue.MALL,"sale", 100));
		db.addLocation(new LocationHandler(203, GlobalVariables.instanceOf().getDate(),14.631665, 121.095702, "CULIDELI INC.", ConstantValue.MALL,"sale", 100));
		db.addLocation(new LocationHandler(204, GlobalVariables.instanceOf().getDate(), 14.631665, 121.095702, "COCINADELI FOOD CORPORATION", ConstantValue.MALL,"sale", 100));
		db.addLocation(new LocationHandler(205, GlobalVariables.instanceOf().getDate(),14.605825,121.083548, "BUMAR MERCHANDISING", ConstantValue.MALL,"sale", 100));
		db.addLocation(new LocationHandler(206, GlobalVariables.instanceOf().getDate(), 14.637462, 121.095951, "SAN RAMON PHARMA DISTRIBUTOR", ConstantValue.MALL,"sale", 100));

		//whole sale
		db.addLocation(new LocationHandler(207, GlobalVariables.instanceOf().getDate(), 14.622272, 121.101451, "JHEARMEL ENTERPRISES", ConstantValue.MALL,"sale", 100));
		db.addLocation(new LocationHandler(208, GlobalVariables.instanceOf().getDate(), 14.648219,121.100145, "NAZEL ELECTRICAL SUPPLY INC.", ConstantValue.MALL,"sale", 100));
		db.addLocation(new LocationHandler(209, GlobalVariables.instanceOf().getDate(), 14.639073, 121.101432, "ANGELICA DL. MOJICA CORPORATION", ConstantValue.MALL,"sale", 100));
		db.addLocation(new LocationHandler(210, GlobalVariables.instanceOf().getDate(), 14.633604,121.087967, "YAKULT MARKETING CORPORATION - MARIKINA", ConstantValue.MALL,"sale", 100));
		db.addLocation(new LocationHandler(211, GlobalVariables.instanceOf().getDate(), 14.622167, 121.096829, "JAMIKCO MARKETING", ConstantValue.MALL,"sale", 100));
		db.addLocation(new LocationHandler(212, GlobalVariables.instanceOf().getDate(), 14.634616, 121.123727, "SURESTEP ENTERPRISE COMPANY", ConstantValue.MALL,"sale", 100));
		db.addLocation(new LocationHandler(213, GlobalVariables.instanceOf().getDate(), 14.656625,121.103522, "DE SUN ENTERPRISE", ConstantValue.MALL,"sale", 100));
		db.addLocation(new LocationHandler(214, GlobalVariables.instanceOf().getDate(), 14.625737, 121.100704, "PABRYCA DE DISENYO INC.", ConstantValue.MALL,"sale", 100));
		db.addLocation(new LocationHandler(215, GlobalVariables.instanceOf().getDate(), 14.648457, 121.119508, "DUO SORELLE, INC..", ConstantValue.MALL,"sale", 100));
		db.addLocation(new LocationHandler(216, GlobalVariables.instanceOf().getDate(),14.660203, 121.113565, "ENER'S GENERAL MERCHANDISE", ConstantValue.MALL,"sale", 100));
		db.addLocation(new LocationHandler(217, GlobalVariables.instanceOf().getDate(), 14.657263, 121.105282, "RIZZADON'S GENERAL MERCHANDISE", ConstantValue.MALL,"sale", 100));
		db.addLocation(new LocationHandler(218, GlobalVariables.instanceOf().getDate(), 14.641246, 121.083581, "BMAISE, INC.", ConstantValue.MALL,"sale", 100));
		db.addLocation(new LocationHandler(219, GlobalVariables.instanceOf().getDate(), 14.630313,121.091486, "RIELLE ENTERPRISES", ConstantValue.MALL,"sale", 100));
		db.addLocation(new LocationHandler(220, GlobalVariables.instanceOf().getDate(), 14.645855, 121.094572, "YMAGE TRADING & CONSTRUCTION CORPORATION", ConstantValue.MALL,"sale", 100));
		db.addLocation(new LocationHandler(221, GlobalVariables.instanceOf().getDate(), 14.652009, 121.113722, "CINCO ENTERPRISES INC.", ConstantValue.MALL,"sale", 100));
		db.addLocation(new LocationHandler(222, GlobalVariables.instanceOf().getDate(), 14.644721, 121.103169, "TOYLANDS UNLIMITED, INC.", ConstantValue.MALL,"sale", 100));
		db.addLocation(new LocationHandler(223, GlobalVariables.instanceOf().getDate(), 14.631297, 121.101633, "TIME FOREIGN TRADE AND INDUSTRY JOINT STOCK COMPANY", ConstantValue.MALL,"sale", 100));
		db.addLocation(new LocationHandler(224, GlobalVariables.instanceOf().getDate(), 14.644256, 121.111445, "TANGENT INTERNATIONALE TRADERS, INC.", ConstantValue.MALL,"sale", 100));
		db.addLocation(new LocationHandler(225, GlobalVariables.instanceOf().getDate(),14.634035,121.085986, "MOTORTRADE NATIONWIDE CORPORATION", ConstantValue.MALL,"sale", 100));
		db.addLocation(new LocationHandler(226, GlobalVariables.instanceOf().getDate(), 14.653509, 121.072774, "JASPAK ENTERPRISES", ConstantValue.MALL,"sale", 100));
		db.addLocation(new LocationHandler(227, GlobalVariables.instanceOf().getDate(),14.673144, 121.094577, "INVINE ENTERPRISES", ConstantValue.MALL,"sale", 100));
		db.addLocation(new LocationHandler(228, GlobalVariables.instanceOf().getDate(),14.648779, 121.107047, "PVM RICE RETAILER & GENERAL MERCHANDISE", ConstantValue.MALL,"sale", 100));
		db.addLocation(new LocationHandler(229, GlobalVariables.instanceOf().getDate(), 14.674116, 120.511125, "RNE RICE CENTER", ConstantValue.MALL,"sale", 100));
		db.addLocation(new LocationHandler(230, GlobalVariables.instanceOf().getDate(),14.590261, 121.104026, "SHOEROOM CORPORATION", ConstantValue.MALL,"sale", 100));
		db.addLocation(new LocationHandler(231, GlobalVariables.instanceOf().getDate(), 14.670965, 121.104407, "SHOEFIT INC.", ConstantValue.MALL,"sale", 100));
		db.addLocation(new LocationHandler(232, GlobalVariables.instanceOf().getDate(), 14.637462, 121.095951, "PHENTOE SHOE SUPPLY", ConstantValue.MALL,"sale", 100));
		db.addLocation(new LocationHandler(233, GlobalVariables.instanceOf().getDate(), 14.631848,121.082454, "COUGAR ATHLETIC TRENDS, INC.", ConstantValue.MALL,"sale", 100));
		db.addLocation(new LocationHandler(234, GlobalVariables.instanceOf().getDate(), 14.590261, 121.104026, "I.L. MOJICA TRADING", ConstantValue.MALL,"sale", 100));
		db.addLocation(new LocationHandler(235, GlobalVariables.instanceOf().getDate(), 14.598559, 121.073578, "LOT LOT GEN MERCHANDISE", ConstantValue.MALL,"sale", 100));
		db.addLocation(new LocationHandler(236, GlobalVariables.instanceOf().getDate(), 14.654523, 121.100455, "GEMMA STORE", ConstantValue.MALL,"sale", 100));
		db.addLocation(new LocationHandler(238, GlobalVariables.instanceOf().getDate(), 14.664989, 121.103535, "ROBEE SOFTDRINKS DEALER", ConstantValue.MALL,"sale", 100));
		db.addLocation(new LocationHandler(239, GlobalVariables.instanceOf().getDate(), 14.656893, 121.111818, "INTENSITY 7", ConstantValue.MALL,"sale", 100));
		db.addLocation(new LocationHandler(240, GlobalVariables.instanceOf().getDate(), 14.669071, 121.106547, "DOTMILES TRADING", ConstantValue.MALL,"sale", 100));
		db.addLocation(new LocationHandler(241, GlobalVariables.instanceOf().getDate(), 14.636184,121.089315, "JMANILA BAY SPINNING MILLS, INC.", ConstantValue.MALL,"sale", 100));
		db.addLocation(new LocationHandler(242, GlobalVariables.instanceOf().getDate(), 14.638804, 121.110944, "CAZEMART INCORPORATED", ConstantValue.MALL,"sale", 100));
		db.addLocation(new LocationHandler(243, GlobalVariables.instanceOf().getDate(), 14.673315, 121.108887, "ELEMENT IND'L BUILDERS, INC.", ConstantValue.MALL,"sale", 100));
		db.addLocation(new LocationHandler(244, GlobalVariables.instanceOf().getDate(),14.631297,121.101633,"PEDIA-VAX MARKETING CORPORATION", ConstantValue.MALL,"sale", 100));
		db.addLocation(new LocationHandler(245, GlobalVariables.instanceOf().getDate(),14.622167,121.096829,"F.G. TRADING CORPORATION", ConstantValue.MALL,"sale", 100));
		db.addLocation(new LocationHandler(246, GlobalVariables.instanceOf().getDate(), 14.625134,121.100734,"MIN SHIANG CORPORATION", ConstantValue.MALL,"sale", 100));
		db.addLocation(new LocationHandler(247, GlobalVariables.instanceOf().getDate(),14.599672,121.075147,"ARCHON NELL INCORPORATED", ConstantValue.MALL,"sale", 100));
		db.addLocation(new LocationHandler(248, GlobalVariables.instanceOf().getDate(), 14.683135,121.072171,"J4G TRADING CORPORATION", ConstantValue.MALL,"sale", 100));
		db.addLocation(new LocationHandler(249, GlobalVariables.instanceOf().getDate(), 14.620705,121.100703,"JFOODS CO.", ConstantValue.MALL,"sale", 100));
		db.addLocation(new LocationHandler(250, GlobalVariables.instanceOf().getDate(), 14.604302,121.026057,"AVON COSMETICS", ConstantValue.MALL,"sale", 100));
		db.addLocation(new LocationHandler(251, GlobalVariables.instanceOf().getDate(), 14.640197,121.094277,"C7 AUTOZONE, INC.", ConstantValue.MALL,"sale", 100));
		db.addLocation(new LocationHandler(252, GlobalVariables.instanceOf().getDate(), 14.648936,121.113394,"ALL SEASON SPORTS INC.", ConstantValue.MALL,"sale", 100));
		db.addLocation(new LocationHandler(253, GlobalVariables.instanceOf().getDate(), 14.641378,121.099246,"YUNPHIL TRADING CORPORATION", ConstantValue.MALL,"sale", 100));
		db.addLocation(new LocationHandler(254, GlobalVariables.instanceOf().getDate(), 14.599672,121.075147,"SERAPHINA INC.", ConstantValue.MALL,"sale", 100));
		db.addLocation(new LocationHandler(255, GlobalVariables.instanceOf().getDate(), 14.646861,121.108176,"CULLINAN GROUP INC.", ConstantValue.MALL,"sale", 100));
		db.addLocation(new LocationHandler(256, GlobalVariables.instanceOf().getDate(), 14.639771,121.118997,"WATERBLOK VIASEAL SYSTEMS CORPORATION", ConstantValue.MALL,"sale", 100));
		db.addLocation(new LocationHandler(257, GlobalVariables.instanceOf().getDate(), 14.620826,121.099428,"THE OTTOMAN TRADING CO. LTD.", ConstantValue.MALL,"sale", 100));
		db.addLocation(new LocationHandler(278, GlobalVariables.instanceOf().getDate(), 14.659235,121.107115,"SUPERLUCK TRADING ", ConstantValue.MALL,"sale", 100));
		db.addLocation(new LocationHandler(279, GlobalVariables.instanceOf().getDate(), 14.673315,121.108887,"SUNDIAL TAPE INDUSTRIES CORPORATION", ConstantValue.MALL,"sale", 100));

		db.addLocation(new LocationHandler(3, GlobalVariables.instanceOf().getDate(), 14.673315,121.108887,"ALLENTECH LABEL CORP.", ConstantValue.MALL,"sale", 100));
		db.addLocation(new LocationHandler(4, GlobalVariables.instanceOf().getDate(), 14.639812,121.109824,"KATIPS  PAINT CENTER", ConstantValue.MALL,"sale", 100));
		db.addLocation(new LocationHandler(4, GlobalVariables.instanceOf().getDate(), 14.636184,121.089315,"TH FIL SCOT TRADERS, INC.", ConstantValue.MALL,"sale", 100));
		db.addLocation(new LocationHandler(3, GlobalVariables.instanceOf().getDate(),14.634389,121.090197,"JAMS BAKELAND FOODS INC.", ConstantValue.MALL,"sale", 100));
		db.addLocation(new LocationHandler(4, GlobalVariables.instanceOf().getDate(), 14.647106,121.106816,"BRODCOM ENTERPRISES", ConstantValue.MALL,"sale", 100));
		db.addLocation(new LocationHandler(4, GlobalVariables.instanceOf().getDate(),14.660257,121.113002,"LOGISTIKOS COMMERCIAL CORPORATION", ConstantValue.MALL,"sale", 100));
		db.addLocation(new LocationHandler(5, GlobalVariables.instanceOf().getDate(), 14.639191,121.117013,"CABESA ENTERPRISES PHILS. INC.", ConstantValue.MALL,"sale", 100));
		db.addLocation(new LocationHandler(1, GlobalVariables.instanceOf().getDate(), 14.645309,121.096364,"BLESSED GB & N MARKETING CORPORATION", ConstantValue.MALL,"sale", 100));
		db.addLocation(new LocationHandler(3, GlobalVariables.instanceOf().getDate(), 14.625405,121.102386,"DRAGON KOI VENTURES, INC.", ConstantValue.MALL,"sale", 100));
		db.addLocation(new LocationHandler(4, GlobalVariables.instanceOf().getDate(), 14.644256,121.111445,"REIFENHAUSER GERMAN TRADING CORPORATION", ConstantValue.MALL,"sale", 100));
		db.addLocation(new LocationHandler(5, GlobalVariables.instanceOf().getDate(), 14.646861,121.108176,"TECHNO TIME RETAILERS CORPORATION", ConstantValue.MALL,"sale", 100));
		db.addLocation(new LocationHandler(1, GlobalVariables.instanceOf().getDate(), 14.669517,121.104787,"GURMANHAR TRADING CORPORATION", ConstantValue.MALL,"sale", 100));
		db.addLocation(new LocationHandler(3, GlobalVariables.instanceOf().getDate(), 14.599672,121.075147,"TECHNOMARINE ENTERPRISES PHILS., INC.", ConstantValue.MALL,"sale", 100));
		db.addLocation(new LocationHandler(4, GlobalVariables.instanceOf().getDate(), 14.657447,121.088122,"CLICK INNOVATION MARKETING", ConstantValue.MALL,"sale", 100));
		db.addLocation(new LocationHandler(3, GlobalVariables.instanceOf().getDate(), 14.648052,121.105106,"POLYCAST CORPORATION", ConstantValue.MALL,"sale", 100));
		db.addLocation(new LocationHandler(4, GlobalVariables.instanceOf().getDate(), 14.638504,121.126421,"ST. PARASKEVA TRADING", ConstantValue.MALL,"sale", 100));

		db.addLocation(new LocationHandler(3, GlobalVariables.instanceOf().getDate(), 14.648436,121.093698,"SIMPLY CHRISTINE ESSENTIALS CORPORATION", ConstantValue.MALL,"sale", 100));
		db.addLocation(new LocationHandler(4, GlobalVariables.instanceOf().getDate(), 14.639812,121.109824,"OSP ADVANTAGE SYSTEM CORPORATION", ConstantValue.MALL,"sale", 100));
		db.addLocation(new LocationHandler(3, GlobalVariables.instanceOf().getDate(), 14.647913,121.102395,"FOOTWORKS MARKETING CORPORATION", ConstantValue.MALL,"sale", 100));
		db.addLocation(new LocationHandler(4, GlobalVariables.instanceOf().getDate(), 14.599304,121.110702,"PCM FIGUERAS TRADING INC.", ConstantValue.MALL,"sale", 100));

	}

	public void addLocationType(){
		db.addLocationType(new LocationTypeHandler(ConstantValue.ESTABLISHMENT, GlobalVariables.instanceOf().getDate(),getBitmap(mActivity, R.drawable.establishments), null, "Establishments", "8B0000", getBitmap(mActivity,R.drawable.establishments_pin)));
		db.addLocationType(new LocationTypeHandler(ConstantValue.BANK, GlobalVariables.instanceOf().getDate(), getBitmap(mActivity, R.drawable.bank_icon), null, "Bank", "BFFF0F",getBitmap(mActivity, R.drawable.bank_pin)));
		db.addLocationType(new LocationTypeHandler(ConstantValue.AMUSEMENT, GlobalVariables.instanceOf().getDate(), getBitmap(mActivity, R.drawable.amusement_icon), null, "Amusement", "FF8A4F",getBitmap(mActivity, R.drawable.amusement_pin)));
		db.addLocationType(new LocationTypeHandler(ConstantValue.MALL, GlobalVariables.instanceOf().getDate(),getBitmap(mActivity, R.drawable.mall_icon), null, "Wholesale", "8B0000", getBitmap(mActivity,R.drawable.mall_pin)));

	}


	public ArrayList<LocationTypeHandler> getAllLocationType(){
		return db.getAllLocationType();
	}
	public ArrayList<LocationHandler> getLocation(int locationType, int specificLoc) {
		ArrayList<LocationHandler> locations = new ArrayList<LocationHandler>();
		if(specificLoc > 0){
			locations.add(db.getLocation(specificLoc));
		}else{
			if(locationType == 0)
				locations  = db.getAllLocation();
			else{
				locations = db.getAllLocationByType(locationType);
			}
		}
		Log.i(TAG, "locations isze " + locations.size());
		return locations;
	}

	public LocationTypeHandler getTypeInfo(int type) {
		return db.getLocationType(type);
	}

	public LocationHandler getLocationData(int locationID) {
		return db.getLocation(locationID);
	}

	public ArrayList<LocationInfoHandler> getLocationInfoData(int locationID) {
		return db.getAllLocationInfoByLocationID(locationID);
	}

	public SubLocationHandler getSubLocationData(int locationID) {
		return db.getSubLocation(locationID);
	}
	public ArrayList<SubLocationHandler> getAllSubLocationData(int locationID) {
		return db.getAllSubLocationByLocationID(locationID);
	}

	public static Bitmap getBitmap(Context context, int image){
		Bitmap bm = BitmapFactory.decodeResource(context.getResources(),image);
		Log.i(TAG, "converting to bitmap : " + image  + " bitmap data " + bm);
		return bm;
	}



}
