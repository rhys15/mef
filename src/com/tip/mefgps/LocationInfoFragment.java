package com.tip.mefgps;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.gms.internal.cn;
import com.google.android.gms.maps.model.LatLng;
import com.tip.mefgps.adapter.LocationInfoAdapter;
import com.tip.mefgps.constant.ConstantValue;
import com.tip.mefgps.constant.GlobalVariables;
import com.tip.mefgps.controller.AddDragMarkerCallback;
import com.tip.mefgps.controller.AddDragableMarker;
import com.tip.mefgps.controller.AddLocationInfoCallBack;
import com.tip.mefgps.controller.AddSubLocationCallBack;
import com.tip.mefgps.database.Database;
import com.tip.mefgps.database.LocationHandler;
import com.tip.mefgps.database.LocationInfoHandler;
import com.tip.mefgps.database.SubLocationHandler;
import com.tip.mefgps.dialogs.AddLocationContentDialog;
import com.tip.mefgps.dialogs.AddSubLocationDialog;
import com.tip.mefgps.model.MapLocationModel;


public class LocationInfoFragment extends Fragment implements OnClickListener{

	private View view;
	private FragmentChangeActivity mainFragment;
	private int locationID, layoutType;
	private final static String LOCATION_ID = "selection";
	private static final String TAG = LocationInfoFragment.class.getSimpleName();
	private static final String LAYOUT_TYPE = "layout_type";
	private ArrayList<LocationInfoHandler> locationInfoData = new ArrayList<LocationInfoHandler>();
	private ArrayList<SubLocationHandler> subLocationData =  new ArrayList<SubLocationHandler>();
	private MapLocationModel mapLocationModel;
	private Database db;
	private ImageView showWay, addSubLocData, addLocationInfo;
	private AddLocationContentDialog locationDataDialog;
	private AddSubLocationDialog subLocationDialog;
	/** Widgets **/
	TextView title;
	ListView  infoList;
	LinearLayout establishmentLayout;
	TextView subLoc[];


	public static final LocationInfoFragment newInstance(int selection, int type){
		LocationInfoFragment fragment = new LocationInfoFragment();
		Bundle bundle = new Bundle();
		bundle.putInt(LOCATION_ID, selection);
		bundle.putInt(LAYOUT_TYPE, type);
		fragment.setArguments(bundle);
		return fragment ;
	}

	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		view = inflater.inflate(R.layout.activity_content, null);
		Bundle bundle = this.getArguments();
		locationID = bundle.getInt(LOCATION_ID, 0);
		layoutType = bundle.getInt(LAYOUT_TYPE, 0);
		db = new Database(getActivity());
		init();
		return view;
	}

	private void init() {
		title = (TextView) view.findViewById(R.id.title);
		infoList = (ListView) view.findViewById(R.id.info_list);
		mapLocationModel = new MapLocationModel(getActivity());
		showWay = (ImageView) view.findViewById(R.id.show_way);
		addLocationInfo =  (ImageView) view.findViewById(R.id.add_loc_info);
		addSubLocData = (ImageView) view.findViewById(R.id.add_sub_loc);
		if(layoutType == ConstantValue.MAIN_LOC)
			setUpContent();
		else
			setUpEstablismentContent();
	}

	private void setUpEstablismentContent() {
		final SubLocationHandler subLocationHandler = mapLocationModel.getSubLocationData(locationID);
		ArrayList<LocationInfoHandler> locationInfoData = mapLocationModel.getLocationInfoData(locationID);

		title.setText(subLocationHandler.getName());
		addLocationInfo.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				locationDataDialog = new AddLocationContentDialog(LocationInfoFragment.this,subLocationHandler.getId(), new AddLocationInfoCallBack() {

					@Override
					public void addData(LocationInfoHandler info) {
						db.addLocationInfo(info);
//						infoList.removeFooterView(footer);

						locationDataDialog.dismiss();
						init();
					}
				});				
			}
		});
		
//		addSubLocData.setOnClickListener(new OnClickListener() {
//			
//			@Override
//			public void onClick(View v) {
//
//				mainFragment.toggle();
//				subLocationDialog = new AddSubLocationDialog(getActivity(), new AddSubLocationCallBack() {
//					
//					@Override
//					public void markerView() {
//						
//						new AddDragableMarker(mainFragment.getMapFrag().getmMap(), ConstantValue.MARIKINA_LONG_LAT, new AddDragMarkerCallback() {
//							
//							@Override
//							public void location(LatLng location) {
//								subLocationDialog.setLocation(location);
//								mainFragment.showSecondaryMenu();
//							}
//						});
//					}
//					
//					@Override
//					public void addLocation(SubLocationHandler subLocation) {
//						db.addSubLocation(subLocation);
////						infoList.removeFooterView(footer);
//						subLocationDialog.dismiss();
//						mainFragment.setMapDisplay(0);
//						init();
//						
//					}
//				}, subLocationHandler);
//			}
//		});
		infoList.setAdapter(new LocationInfoAdapter(getActivity(), locationInfoData));
	}

	private void setUpContent() {

		final LocationHandler locationData = mapLocationModel.getLocationData(locationID);
		locationInfoData = mapLocationModel.getLocationInfoData(locationID);
		subLocationData = mapLocationModel.getAllSubLocationData(locationID);

		title.setText(locationData.getName());

		/**Set visibitliy of establishments Layout **/
		View header = LayoutInflater.from(getActivity()).inflate(R.layout.view_paralax_header, null);
		final View footer = LayoutInflater.from(getActivity()).inflate(R.layout.footer_add_info, null);
		establishmentLayout = (LinearLayout) header.findViewById(R.id.establishments_layout);
		establishmentLayout.setVisibility(View.GONE);
		if(subLocationData.size() > 0){
			subLoc = new TextView[subLocationData.size()];
			establishmentLayout.setVisibility(View.VISIBLE);
			int itemCount = 0;
			for(SubLocationHandler item : subLocationData ){
				subLoc[itemCount] = new TextView(getActivity());
				subLoc[itemCount].setText(item.getName());
				subLoc[itemCount].setId(itemCount);
				subLoc[itemCount].setPadding(5, 0, 0, 0);
				//				subLoc[itemCount].setTextColor(getActivity().getResources().getColor(R.color.));
				subLoc[itemCount].setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.font_small));
				establishmentLayout.addView(subLoc[itemCount]);
				subLoc[itemCount].setOnClickListener(this);
				itemCount++;
			}

		}
		infoList.addHeaderView(header);
//		infoList.addFooterView(footer);

//		footer.findViewById(R.id.add_location_info).setOnClickListener(new OnClickListener() {
//
//			@Override
//			public void onClick(View v) {
//				
//			}
//		});
		addLocationInfo.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				locationDataDialog = new AddLocationContentDialog(LocationInfoFragment.this,locationData.getId(), new AddLocationInfoCallBack() {

					@Override
					public void addData(LocationInfoHandler info) {
						db.addLocationInfo(info);
//						infoList.removeFooterView(footer);

						locationDataDialog.dismiss();
						init();
					}
				});				
			}
		});
		
		addSubLocData.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {

				mainFragment.toggle();
				subLocationDialog = new AddSubLocationDialog(getActivity(), new AddSubLocationCallBack() {
					
					@Override
					public void markerView() {
						
						new AddDragableMarker(mainFragment.getMapFrag().getmMap(), ConstantValue.MARIKINA_LONG_LAT, new AddDragMarkerCallback() {
							
							@Override
							public void location(LatLng location) {
								subLocationDialog.setLocation(location);
								mainFragment.showSecondaryMenu();
							}
						});
					}
					
					@Override
					public void addLocation(SubLocationHandler subLocation) {
						db.addSubLocation(subLocation);
//						infoList.removeFooterView(footer);
						subLocationDialog.dismiss();
						mainFragment.setMapDisplay(0);
						init();
						
					}
				}, locationData);
			}
		});
//		footer.findViewById(R.id.add_sub_loc).setOnClickListener(new OnClickListener() {
//
//			@Override
//			public void onClick(View v) {
//				mainFragment.toggle();
//				subLocationDialog = new AddSubLocationDialog(getActivity(), new AddSubLocationCallBack() {
//					
//					@Override
//					public void markerView() {
//						
//						new AddDragableMarker(mainFragment.getMapFrag().getmMap(), ConstantValue.MARIKINA_LONG_LAT, new AddDragMarkerCallback() {
//							
//							@Override
//							public void location(LatLng location) {
//								subLocationDialog.setLocation(location);
//								mainFragment.showSecondaryMenu();
//							}
//						});
//					}
//					
//					@Override
//					public void addLocation(SubLocationHandler subLocation) {
//						db.addSubLocation(subLocation);
//						infoList.removeFooterView(footer);
//						subLocationDialog.dismiss();
//						mainFragment.setMapDisplay(0);
//						init();
//						
//					}
//				}, locationData);
//			}
//		});

		infoList.setAdapter(new LocationInfoAdapter(getActivity(), locationInfoData));

		view.findViewById(R.id.show_way).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				mainFragment.showWay(locationData);
				mainFragment.getSlidingMenu().toggle();
			}
		});
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		mainFragment = (FragmentChangeActivity) activity;
	}

	@Override
	public void onClick(View v) {
		for (int i = 0; i < subLocationData.size(); i++) {
			if(v.getId() == i){
				int id = subLocationData.get(i).getId();
				mainFragment.setContent(LocationInfoFragment.newInstance(id, ConstantValue.SUB_LOC));
				mainFragment.showSecondaryMenu();
			}
		}

	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		Log.i(TAG, "on result : " + requestCode);
		if (resultCode == Activity.RESULT_OK) {
			String single_path = data.getStringExtra("single_path");
			Bitmap bitmap = null;
			switch (requestCode) {
			case ConstantValue.CATEGORY_LOCATION_ICON:
				bitmap = BitmapFactory.decodeFile(single_path);
				bitmap = GlobalVariables.scaleBitmap(bitmap, 50);
				locationDataDialog.setImageBit(bitmap);
				break;

			default:
				break;
			}
		}
	}

}
