package com.tip.mefgps.controller;

import com.google.android.gms.maps.model.LatLng;

public interface AddDragMarkerCallback {
	public void location(LatLng location);
}
