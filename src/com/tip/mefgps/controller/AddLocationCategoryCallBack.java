package com.tip.mefgps.controller;

import android.graphics.Bitmap;

public interface AddLocationCategoryCallBack {
	public void addData(String categoryName, Bitmap icon, Bitmap pin);
}
