package com.tip.mefgps.controller;

import com.tip.mefgps.database.LocationHandler;


public interface AddLocationCallBack {
	public void addLocation(LocationHandler location);
	public void markerView();
}
