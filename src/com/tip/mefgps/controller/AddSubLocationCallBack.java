package com.tip.mefgps.controller;

import com.tip.mefgps.database.SubLocationHandler;


public interface AddSubLocationCallBack {
	public void addLocation(SubLocationHandler location);
	public void markerView();
}
