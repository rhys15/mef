package com.tip.mefgps.controller;

import android.app.Activity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.ListView;

import com.tip.mefgps.FragmentChangeActivity;
import com.tip.mefgps.MainMenu;
import com.tip.mefgps.R;
import com.tip.mefgps.adapter.SearchAdapter;
import com.tip.mefgps.constant.ConstantValue;
import com.tip.mefgps.constant.GlobalVariables;
import com.tip.mefgps.database.Database;
import com.tip.mefgps.database.LocationHandler;
import com.tip.mefgps.model.GPSTracker;

public class SearchController {
	protected static final String TAG = SearchController.class.getSimpleName();
	private Activity activity;
	private View view;
	private ListView list;
	private EditText seachEt;
	private SearchAdapter searchAdapter;
	private Database db;
	private FragmentChangeActivity fragmentChangeActivity;
	private GPSTracker gpsData;
	public SearchController(Activity activity, View view) {
		this.activity = activity;
		this.view = view;
		db = new Database(activity);
		fragmentChangeActivity = (FragmentChangeActivity) activity;
		gpsData = new GPSTracker(activity);
	}

	public void setVariables(){
		list  = (ListView) view.findViewById(R.id.listview);
		seachEt = (EditText) view.findViewById(R.id.search);
		searchAdapter = new SearchAdapter(activity, db.getAllLocation());
		list.setAdapter(searchAdapter);
	}
	
	public void setFunctions(){
		seachEt.addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				String searchString = seachEt.getText().toString();
				if(searchString.contains("nearest")){
					Log.i(TAG, "near found");
					searchString  = searchString.replace("nearest", "");
					searchString  = searchString.replaceAll("\\s", "");
					searchAdapter.setData(db.findNear(searchString, gpsData.getLatitude(), gpsData.getLongitude()));
				}else if(searchString.contains("near")){
					searchString  = searchString.replace("nearest", "");
					searchString  = searchString.replaceAll("\\s", "");
					searchAdapter.setData(db.findNear(searchString, gpsData.getLatitude(), gpsData.getLongitude()));
				}else{
					searchAdapter.setData(db.getAllLocationFilter(seachEt.getText().toString()));
				}
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				
			}
			
			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				
			}
		});
		list.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position,
					long arg3) {
				LocationHandler locData = searchAdapter.getItem(position);
				Log.i(TAG, "on click ");
				fragmentChangeActivity.displayLocation(locData.getId()); 
				fragmentChangeActivity.getSlidingMenu().toggle();
			}
		});
		
	}
}

