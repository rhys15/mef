package com.tip.mefgps.controller;

import com.tip.mefgps.database.LocationInfoHandler;

import android.graphics.Bitmap;

public interface AddLocationInfoCallBack {
	public void addData(LocationInfoHandler info);
}
