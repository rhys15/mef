package com.tip.mefgps.adapter;

import android.app.Activity;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.maps.GoogleMap.InfoWindowAdapter;
import com.google.android.gms.maps.model.Marker;
import com.tip.mefgps.R;
import com.tip.mefgps.controller.MapLocationController;
import com.tip.mefgps.database.Database;
import com.tip.mefgps.database.LocationHandler;
import com.tip.mefgps.database.LocationTypeHandler;

public class MapWindowAdapter implements InfoWindowAdapter{
	private static final String TAG = MapWindowAdapter.class.getSimpleName();
	private MapLocationController mapLocationController;
	private  Activity mActivity;
	private View view;
	private Database db;
	public MapWindowAdapter(MapLocationController mapLocationController, Activity mActivity) {
		this.mapLocationController = mapLocationController;
		this.mActivity = mActivity;
		db= new Database(mActivity);
		view = mActivity.getLayoutInflater().inflate(R.layout.item_map_info, null);
	}

	@Override
	public View getInfoContents(Marker marker) {
		try {
			LocationHandler locData = mapLocationController.getMarkers().get(marker.getId());
			if(locData != null){

				TextView title = (TextView) view.findViewById(R.id.title);
				TextView location = (TextView) view.findViewById(R.id.long_lat);
				TextView type = (TextView) view.findViewById(R.id.location_type);
				title.setText(locData.getName());
				Log.i(TAG, "loc id : " + locData.getType());

				for(LocationTypeHandler item: db.getAllLocationType()){
					Log.i(TAG, "location types : " + item.getId() + " = " + locData.getType());
				}
				LocationTypeHandler locationType = db.getLocationType(locData.getType());
				location.setText(locData.getLatitude() + " " + locData.getLongitude());
				type.setText(locationType.getName());
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		

		return view;
	}

	@Override
	public View getInfoWindow(Marker marker) {
		// TODO Auto-generated method stub
		return null;
	}

}
